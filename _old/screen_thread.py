from threading import Thread
import pygame as pg


SNAP_PER_SEC = 200


class ScreenThread(Thread):
    def __init__(self, screen):
        Thread.__init__(self)
        self.screen = screen

    def run(self):
        while 1:
            for i in pg.event.get():
                if i.type == pg.QUIT:
                    exit()
            pg.time.delay(int(1 / SNAP_PER_SEC * 1000))
            pg.display.update()
