from dataclasses import asdict
from pathlib import Path
from time import sleep

import pygame as pg
from pygame import mixer
import time

import yaml

from models.timeline import Checkpoint, Timeline, Mode, Song


class Tile(pg.sprite.Sprite):
    def __init__(self, position):
        super().__init__()
        self.image = pg.Surface((500, 500))
        self.color = pg.Color((0, 0, 0))
        self.image.fill(self.color)
        self.rect = self.image.get_rect(topleft=position)

    def change_color(self, color):
        self.color = color
        self.image.fill(self.color)


group = [
    Checkpoint(level=100, beats=2),
    Checkpoint(level=0, beats=2, mode=Mode.LINEAR),
    Checkpoint(level=50, beats=1),
    Checkpoint(level=20, beats=1),
    Checkpoint(level=50, beats=1),
    Checkpoint(level=20, beats=1),
]

checkpoints = [
    Checkpoint(level=100, beats=0),
    Checkpoint(level=0, beats=2, mode=Mode.LINEAR),
    Checkpoint(level=50, beats=2),
    Checkpoint(level=20, beats=2),
    Checkpoint(level=50, beats=2),
] + group * 10

t = Timeline(name="Led1", checkpoints=checkpoints)
s = Song(file=Path("data/EVO - Стать мечтой.mp3"), start_beat=2.5, bpm=149, timelines=[t])

SNAP_PER_SEC = 100

with open("../timeline_example.yaml", "w") as file:
    documents = yaml.dump(asdict(s), file)

# Инициализация PyGame
pg.init()
# Параметры окна
size = [500, 500]
screen = pg.display.set_mode(size)
screen.fill((0, 0, 0))
circle = Tile((0, 0))
screen.blit(circle.image, circle.rect)
pg.display.flip()

mixer.init()
mixer.music.load(str(s.file))
mixer.music.play()
# ждём начало песни start_timestamp
time_between_beats = 60_000 / s.bpm  # in milliseconds
time_to_start = time_between_beats * s.start_beat
millis = int(round(time.time() * 1000))
while True:
    if int(round(time.time() * 1000)) - millis >= time_to_start:
        break
# начинаем проигрывать цветомузыку по таймлайну
for n, p1 in enumerate(checkpoints[:-1]):
    p2 = checkpoints[n + 1]
    delta_time = p2.beats * time_between_beats
    millis = int(round(time.time() * 1000))
    if p2.mode == Mode.LINEAR:
        delta_level = p2.level - p1.level
        step_level = delta_level * 1000 / SNAP_PER_SEC / delta_time
        amount_of_steps = delta_time * SNAP_PER_SEC / 1000
        cur_level = p1.level
        for i in range(int(amount_of_steps)):
            cur_level += step_level
            # print('■'*int(cur_level))
            circle.change_color(pg.Color((int(cur_level * 255 / 100), 0, 0)))
            screen.blit(circle.image, circle.rect)
            pg.display.update()
            if (
                int(round(time.time() * 1000)) - millis >= delta_time
            ):  # для корректировки, более своевременного выхода из цикла
                break
            sleep(1 / SNAP_PER_SEC)
    else:
        while True:
            if int(round(time.time() * 1000)) - millis >= delta_time:
                break
        circle.change_color(pg.Color(int(p2.level * 255 / 100), 0, 0))
        screen.blit(circle.image, circle.rect)
        pg.display.update()
        sleep(1 / SNAP_PER_SEC)
