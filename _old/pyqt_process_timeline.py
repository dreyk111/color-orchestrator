import time
from threading import Thread

from PyQt5.QtGui import QColor

from models.timeline import Timeline, Song, Mode

SNAP_PER_SEC = 500


class PlayTimelineThread(Thread):
    def __init__(self, s: Song, rect, rect_color_hue, thread_name):
        Thread.__init__(self)
        self.s = s
        self.rect = rect
        self.color_hue = rect_color_hue
        self.thread_name = thread_name
        self.stop_flag = True

    def run_timeline(self, timeline: Timeline, additional_beat_shift=0):
        self.timeline = timeline
        self.start_from_beat = additional_beat_shift
        self.stop_flag = False
        self.start_millis = time.time()  # in seconds
        self.sum_delta_from_start = 0  # Sum of played steps in seconds

    def run(self):
        while True:
            while True:
                if not self.stop_flag:
                    break
                else:
                    time.sleep(.001)

            time_between_beats = 60 / self.s.bpm  # in seconds
            checkpoints = self.timeline.checkpoints
            if not checkpoints:
                break
            if not self.start_from_beat:
                # start_from_beat = 0, играем с самого начала
                # ждём начало песни start_timestamp
                # В режиме просмотра таймлайнов ждать до начала никогда не надо! Мы уже стартуем с первого бита!
                # delta_time = time_between_beats * self.s.start_beat
                # wait_to_time_millis = self.start_millis + delta_time
                # time.sleep(wait_to_time_millis - time.time())
                # self.sum_delta_from_start += delta_time
                pass
            else:
                # если start_from_beat = 1 - начинаем с 1-го бита
                sum_beats_from_start = checkpoints[0].beats
                if sum_beats_from_start >= self.start_from_beat:
                    start_checkpoint_num = 0
                    checkpoints[0].beats = sum_beats_from_start - self.start_from_beat
                else:
                    start_checkpoint_num = len(checkpoints) - 1
                    for n, p1 in enumerate(checkpoints[:-1]):
                        p2 = checkpoints[n+1]
                        if sum_beats_from_start + p2.beats >= self.start_from_beat:
                            start_checkpoint_num = n
                            p1.beats = sum_beats_from_start-self.start_from_beat
                            break
                        else:
                            sum_beats_from_start += p2.beats
                checkpoints = checkpoints[start_checkpoint_num:]
            if not checkpoints:
                break
            # отображаем нужный уровень стартого бита
            self.rect.setBrush(QColor.fromHsl(self.color_hue, 140, int(checkpoints[0].level * 160 / 100)))

            for n, p1 in enumerate(checkpoints):
                if n == len(checkpoints) - 1:  # last Checkpoint
                    next_checkpoint_level = 0
                else:
                    next_checkpoint_level = checkpoints[n + 1].level

                delta_time = p1.beats * time_between_beats
                wait_to_time_millis = self.start_millis + self.sum_delta_from_start + delta_time  # seconds timestamp
                if p1.mode == Mode.LINEAR:
                    delta_level = next_checkpoint_level - p1.level
                    amount_of_steps = int(delta_time * SNAP_PER_SEC)
                    level_step = delta_level / amount_of_steps
                    cur_level = p1.level
                    for i in range(amount_of_steps):
                        cur_level += level_step
                        self.rect.setBrush(QColor.fromHsl(self.color_hue, 140, int(cur_level * 160 / 100)))
                        if time.time() - wait_to_time_millis >= 0:
                            break  # для корректировки, более своевременного выхода из цикла
                        time.sleep(1 / SNAP_PER_SEC)
                else:
                    time_to_sleep = wait_to_time_millis - time.time()
                    if time_to_sleep > 0:
                        time.sleep(time_to_sleep)

                self.rect.setBrush(QColor.fromHsl(self.color_hue, 140, int(next_checkpoint_level * 160 / 100)))
                self.sum_delta_from_start += delta_time
                if self.stop_flag:
                    break
            # Timeline закончился
            self.stop_flag = True
