from pygame import mixer
import RPi.GPIO as GPIO
import time
from data.evo_with_editor import s
from _old.pyqt_process_timeline import PlayTimelineThread

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
p11 = GPIO.PWM(11, 100)
p12 = GPIO.PWM(12, 100)
p12.start(0)
p11.start(0)

timelines_mapping = {"Левая рука": 11, "Правая рука": 12}

filename = str(s.file)

mixer.init()
mixer.music.load(filename)

mixer.music.play()
# задержка: 
time.sleep(0.4)
# начинаем проигрывать цветомузыку по асинхронным таймлайнам
threads = [
    PlayTimelineThread(s.timelines[0], s, p11, "Левая рука"), PlayTimelineThread(s.timelines[1], s, p12, "Правая рука")]
for thread in threads:
    thread.start()
