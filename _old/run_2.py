from PyQt5 import QtCore, QtWidgets, QtMultimedia
from data.evo_simple import s
from _old.pyqt_process_timeline import PlayTimelineThread


class GraphicsScene(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super(GraphicsScene, self).__init__(QtCore.QRectF(0, 0, 1000, 1000), parent)
        self._start = QtCore.QPointF()
        self._current_rect_item = None

    def addMyRect(self, x, y, width, height, color):
        self._current_rect_item = QtWidgets.QGraphicsRectItem()
        self._current_rect_item.setBrush(color)
        self._current_rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
        self._current_rect_item.setRect(QtCore.QRectF(x, y, width, height))
        self.addItem(self._current_rect_item)
        return self._current_rect_item


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        scene = GraphicsScene(self)
        view = QtWidgets.QGraphicsView(scene)
        self.setCentralWidget(view)

        r1 = scene.addMyRect(20, 20, 300, 300, QtCore.Qt.darkBlue)
        r2 = scene.addMyRect(340, 20, 300, 300, QtCore.Qt.red)
        self.rects = [r1, r2]


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    # w.resize(640, 480)
    w.show()
    filename = str(s.file)
    music_file_path = QtCore.QDir.current().absoluteFilePath(filename)
    media = QtCore.QUrl.fromLocalFile(music_file_path)
    content = QtMultimedia.QMediaContent(media)
    player = QtMultimedia.QMediaPlayer()
    player.setMedia(content)
    player.play()
    # начинаем проигрывать цветомузыку по асинхронным таймлайнам
    threads = [PlayTimelineThread(t, s, w.rects[num], 310) for num, t in enumerate(s.timelines)]
    for thread in threads:
        thread.start()
    sys.exit(app.exec_())
