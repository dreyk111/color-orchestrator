import time
from threading import Thread

from models.timeline import Timeline, Song, Mode
import pygame as pg

SNAP_PER_SEC = 500


class PlayTimelineThread(Thread):
    def __init__(self, timeline: Timeline, s: Song, circle, screen):
        Thread.__init__(self)
        self.timeline = timeline
        self.s = s
        self.circle = circle
        self.screen = screen

    def run(self):
        time_between_beats = 60_000 / self.s.bpm  # in milliseconds
        # начинаем проигрывать цветомузыку по таймлайну
        checkpoints = self.timeline.checkpoints
        for n, p1 in enumerate(checkpoints[:-1]):
            p2 = checkpoints[n + 1]
            delta_time = p2.beats * time_between_beats
            millis = (time.time() * 1000)
            if p2.mode == Mode.LINEAR:
                delta_level = p2.level - p1.level
                step_level = delta_level * 1000 / SNAP_PER_SEC / delta_time
                amount_of_steps = delta_time * SNAP_PER_SEC / 1000
                cur_level = p1.level
                for i in range(int(amount_of_steps)):
                    cur_level += step_level
                    self.circle.change_color(pg.Color(int(cur_level * 255 / 100), 0, 0))
                    self.screen.blit(self.circle.image, self.circle.rect)
                    pg.display.update()
                    if (
                            time.time() * 1000 - millis >= delta_time
                    ):  # для корректировки, более своевременного выхода из цикла
                        break
                    pg.time.delay(int(1 / SNAP_PER_SEC * 1000))
            else:
                while True:
                    if (time.time() * 1000) - millis >= delta_time:
                        break
                    pg.time.delay(int(1 / SNAP_PER_SEC * 1000))
                self.circle.change_color(pg.Color(int(p2.level * 255 / 100), 0, 0))
                self.screen.blit(self.circle.image, self.circle.rect)
                pg.display.update()
                pg.time.delay(int(1 / SNAP_PER_SEC * 1000))
