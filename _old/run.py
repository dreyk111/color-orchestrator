import time

from _old.pygame_objects import Tile
from _old.process_timeline import PlayTimelineThread
import pygame as pg
from data.different_world import s
# Инициализация PyGame
from _old.screen_thread import ScreenThread

pg.init()
# Параметры окна
size = [500, 500]
screen = pg.display.set_mode(size)
screen.fill((0, 0, 0))
circles = [Tile((0, 0)), Tile((250, 0))]
for c in circles:
    screen.blit(c.image, c.rect)
pg.display.flip()

ScreenThread(screen).start()

pg.mixer.init()
pg.mixer.music.load(str(s.file))
pg.mixer.music.play()
# ждём начало песни start_timestamp
time_between_beats = 60_000 / s.bpm  # in milliseconds
time_to_start = time_between_beats * s.start_beat
millis = int(round(time.time() * 1000))
while True:
    if int(round(time.time() * 1000)) - millis >= time_to_start:
        break
# начинаем проигрывать цветомузыку по асинхронным таймлайнам
threads = [PlayTimelineThread(t, s, circles[num], screen) for num, t in enumerate(s.timelines)]
for thread in threads:
    thread.start()
