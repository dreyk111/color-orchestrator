import colorsys
from copy import deepcopy
from typing import List

import numpy as np

from models.timeline import Timeline, Checkpoint, Mode, HSVColor


class LedStripFramesBuilder:
    def __init__(self, timelines: List[Timeline], additional_beat_shift: float, parts_margin: int, len_of_part: int, parts_number: int, frames_per_second: int, bpm: int, color_cls: type):
        self.timelines = deepcopy(timelines)
        self.start_from_beat = additional_beat_shift
        self.parts_margin = parts_margin
        self.len_of_part = len_of_part
        self.parts_number = parts_number
        self.frames_per_second = frames_per_second
        self.bpm = bpm
        self.color_cls = color_cls

        self.len_of_frame_in_seconds = 1/self.frames_per_second
        self.len_of_beat_in_seconds = 60/self.bpm
        print("len_of_frame_in_seconds:", self.len_of_frame_in_seconds)
        print("len_of_beat_in_seconds:", self.len_of_beat_in_seconds)
        self.strip_len = parts_number * (len_of_part + parts_margin)

    def build(self) -> np.array:
        len_of_every_timeline_in_beats = [sum(checkpoint.beats for checkpoint in timeline.checkpoints) for timeline in self.timelines]
        max_len_of_timeline_in_frames = max(int(len_of_timeline_in_beats * self.len_of_beat_in_seconds / self.len_of_frame_in_seconds) for len_of_timeline_in_beats in len_of_every_timeline_in_beats)
        print("max_len_of_timeline_in_frames:", max_len_of_timeline_in_frames)
        result_color_by_frame_and_led_number = np.full((max_len_of_timeline_in_frames, self.strip_len), self.color_cls(0, 0, 0))
        for timeline_index, timeline in enumerate(self.timelines):
            start_led_index = timeline_index * (self.len_of_part + self.parts_margin)
            subarray_for_strip_part = self._build_for_timeline(timeline.checkpoints)
            # Размерность вставляемой матрицы
            m, n = subarray_for_strip_part.shape
            # Координаты начала вставки
            row, col = 0, start_led_index
            # Вставка матрицы
            result_color_by_frame_and_led_number[row:row + m, col:col + n] = subarray_for_strip_part
        return result_color_by_frame_and_led_number

    def _build_for_timeline(self, checkpoints: List[Checkpoint]):
        len_of_timeline_in_beats = sum(checkpoint.beats for checkpoint in checkpoints)
        len_of_timeline_in_frames = int(len_of_timeline_in_beats * self.len_of_beat_in_seconds / self.len_of_frame_in_seconds)

        previous_checkpoints_len_in_beats: float = 0.0
        color_by_frame_and_led_number = np.full((len_of_timeline_in_frames, self.len_of_part), self.color_cls(0,0,0))
        for checkpoint in checkpoints:
            # Длина одного бита в секундах
            start_time = previous_checkpoints_len_in_beats * self.len_of_beat_in_seconds
            start_frame_index = int(start_time / self.len_of_frame_in_seconds)
            # Целое количество кадров, которое влезает в данный отрезок-паттерн
            len_in_frames = int(checkpoint.beats * self.len_of_beat_in_seconds / self.len_of_frame_in_seconds)
            end_frame_index = start_frame_index + len_in_frames
            rpi_color = self._hsv_to_rpi_color(checkpoint.settings.color.h, checkpoint.settings.color.s, checkpoint.settings.color.v)

            if checkpoint.settings.mode is Mode.CONSTANT:
                color_by_frame_and_led_number[start_frame_index:end_frame_index] = rpi_color
            elif checkpoint.settings.mode is Mode.CHASE:
                amount_of_chase_parts = len_in_frames // 3
                reminder_amount_of_frames = len_in_frames % 3
                sub_arr = self._theaterChasePatternForPart(rpi_color)
                a = [sub_arr] * amount_of_chase_parts
                b = sub_arr[:reminder_amount_of_frames]
                color_by_frame_and_led_number[start_frame_index:end_frame_index] = np.concatenate([*a, b])
            elif checkpoint.settings.mode is Mode.FADE_IN:
                sub_arr = self._fadeInPatternForPart(checkpoint.settings.color, len_in_frames)
                color_by_frame_and_led_number[start_frame_index:end_frame_index] = sub_arr
            elif checkpoint.settings.mode is Mode.RUNNING_BEAM_UP:
                sub_arr = self._runningUpBeamPatternForPart(checkpoint.settings.color, len_in_frames)
                color_by_frame_and_led_number[start_frame_index:end_frame_index] = sub_arr

            previous_checkpoints_len_in_beats += checkpoint.beats
        return color_by_frame_and_led_number

    def _hsv_to_rpi_color(self, h: int, s: int, l: int):
        """
        h: 0..359
        s: 0..100
        l: 0..100
        """
        (r_float, g_float, b_float) = colorsys.hsv_to_rgb(h/360, s/255, l/255)
        (r, g, b) = (r_float*255, g_float*255, b_float*255)
        return self.color_cls(int(r),int(g),int(b))

    def _theaterChasePatternForPart(self, color) -> np.array:
        """
        value - color
        time points -> 1 0 0 1 0 0
                       0 1 0 0 1 0
                       0 0 1 0 0 1
        led number \|
        Генерируется всегда 4 фрейма! Нужно далее их зациклить
        """
        PATTERN_BY_TIME_AND_LED_NUMBER = np.full((3, self.len_of_part), self.color_cls(0,0,0))
        for q in range(3):
            # выключаем предыдущий ряд через 3
            if q > 0:
                for i in range(0, self.len_of_part, 3):
                    print(q, i)
                    if i + q-1 < self.len_of_part:
                        PATTERN_BY_TIME_AND_LED_NUMBER[q, i + q-1] = self.color_cls(0,0,0)
            # включаем следующий ряд через 3
            for i in range(0, self.len_of_part, 3):
                if i+q < self.len_of_part:
                    PATTERN_BY_TIME_AND_LED_NUMBER[q, i+q] = color
        return PATTERN_BY_TIME_AND_LED_NUMBER

    def _fadeInPatternForPart(self, color: HSVColor, len_in_frames: int) -> np.array:
        """
        value - color
        time points -> 0 0 0
                       0.5 0.5 0.5
                       1 1 1
        led number \|
        """
        PATTERN_BY_TIME_AND_LED_NUMBER = np.full((len_in_frames, self.len_of_part), self.color_cls(0,0,0))
        float_delta_brightness = 255 / len_in_frames
        current_brightness = 0
        for frame in range(len_in_frames):
            rpi_color = self._hsv_to_rpi_color(color.h, color.s, int(current_brightness))
            for i in range(0, self.len_of_part):
                PATTERN_BY_TIME_AND_LED_NUMBER[frame, i] = rpi_color
            current_brightness += float_delta_brightness
        return PATTERN_BY_TIME_AND_LED_NUMBER

    def _runningUpBeamPatternForPart(self, color: HSVColor, len_in_frames: int) -> np.array:
        """
        value - color
        time points -> 1 0 0
                       0 1 0
                       0 0 1
        led number \|
        """
        PATTERN_BY_TIME_AND_LED_NUMBER = np.full((len_in_frames, self.len_of_part), self.color_cls(0,0,0))
        float_delta_led_number = self.len_of_part / len_in_frames
        rpi_color = self._hsv_to_rpi_color(color.h, color.s, color.v)
        current_len_number = 0
        for frame in range(len_in_frames):
            PATTERN_BY_TIME_AND_LED_NUMBER[frame, int(current_len_number)] = rpi_color
            current_len_number += float_delta_led_number
        return PATTERN_BY_TIME_AND_LED_NUMBER


if __name__ == "__main__":
    from models.timeline import CheckpointSettings, HSVColor
    from dataclasses import dataclass


    @dataclass
    class MyColor:
        r: int
        g: int
        b: int

    timelines = [Timeline(name="1", checkpoints=[
        Checkpoint(beats=5, level=100, settings=CheckpointSettings(color=HSVColor(359,255,255), mode=Mode.CONSTANT)),
        Checkpoint(beats=5, level=100, settings=CheckpointSettings(color=HSVColor(0, 0, 0), mode=Mode.CHASE)),
        Checkpoint(beats=5, level=100, settings=CheckpointSettings(color=HSVColor(100, 255, 255), mode=Mode.CHASE, speed_in_beats=2)),
    ]),
                 Timeline(name="2", checkpoints=[
                     Checkpoint(beats=5, level=100,
                                settings=CheckpointSettings(color=HSVColor(359, 255, 255), mode=Mode.CONSTANT)),
                     Checkpoint(beats=5, level=100,
                                settings=CheckpointSettings(color=HSVColor(0, 0, 0), mode=Mode.CONSTANT)),
                     Checkpoint(beats=5, level=100,
                                settings=CheckpointSettings(color=HSVColor(100, 255, 255), mode=Mode.CONSTANT)),
                 ])
    ]
    builder = LedStripFramesBuilder(timelines=timelines, additional_beat_shift=0, parts_margin=2, len_of_part=5, parts_number=3, frames_per_second=40, bpm=10, color_cls=MyColor)
    result = builder.build()
    print("Yay")
