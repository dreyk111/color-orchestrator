import sys

from PyQt5 import uic, QtWidgets
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsView, QGraphicsItem
from PyQt5.QtGui import QPen, QBrush
from PyQt5.Qt import Qt


class GraphicsScene(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super(GraphicsScene, self).__init__(QtCore.QRectF(-500, -500, 1000, 1000), parent)
        self._start = QtCore.QPointF()
        self._current_rect_item = None
        self.timeline_points = []

    def mousePressEvent(self, event):
        if self.itemAt(event.scenePos(), QtGui.QTransform()) is None:
            self._current_rect_item = QtWidgets.QGraphicsRectItem()
            self._current_rect_item.setBrush(QtCore.Qt.red)
            self._current_rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
            self.addItem(self._current_rect_item)
            self._start = event.scenePos()
            r = QtCore.QRectF(self._start, self._start)
            self._current_rect_item.setRect(r)
        super(GraphicsScene, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if self._current_rect_item is not None:
            r = QtCore.QRectF(self._start, event.scenePos()).normalized()
            self._current_rect_item.setRect(r)
        super(GraphicsScene, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        for number, qrect in enumerate(sorted(self.items(), key=lambda item: item.rect().x())):
            rect = qrect.rect()
            print(number, ":", rect.x(), rect.y(), rect.width(), rect.height())
        self._current_rect_item = None
        super(GraphicsScene, self).mouseReleaseEvent(event)


class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        uic.loadUi('../color_editor.ui', self)
        self.title = "PyQt5 QGraphicView"

        self.InitWindow()
        self.points = QtGui.QPolygon()

    def InitWindow(self):
        self.createGraphicView()
        self.show()

    def createGraphicView(self):
        self.scene = GraphicsScene()
        self.greenBrush = QBrush(Qt.green)
        self.grayBrush = QBrush(Qt.gray)
        self.pen = QPen(Qt.red)
        self.graphicsView.setScene(self.scene)


App = QApplication(sys.argv)
window = Window()
sys.exit(App.exec())
