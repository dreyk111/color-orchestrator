# Подключение библиотеки PyGame
import pygame as pg


class Tile(pg.sprite.Sprite):
    def __init__(self, position):
        super().__init__()
        self.image = pg.Surface((200, 200))
        self.color = pg.Color(0, 200, 90)
        self.image.fill(self.color)
        self.rect = self.image.get_rect(topleft=position)

    def change_color(self, color):
        self.color = color
        self.image.fill(self.color)


# Инициализация PyGame
pg.init()
# Параметры окна
size = [500, 500]
screen = pg.display.set_mode(size)
screen.fill((0, 0, 0))
circle = Tile((200, 200))
screen.blit(circle.image, circle.rect)
pg.display.flip()
FPS = 30
fpsClock = pg.time.Clock()
# Цикл игры
runGame = True  # флаг выхода из цикла игры
alpha = 0
while runGame:
    fpsClock.tick(FPS)  # frame rate
    # Отслеживание события: "закрыть окно"
    for event in pg.event.get():
        if event.type == pg.QUIT:
            runGame = False
    circle.change_color(pg.Color(0, alpha, 0))
    screen.blit(circle.image, circle.rect)
    pg.display.update()
    alpha += 1
    if alpha == 200:
        alpha = 0

# Выход из игры:
pg.quit()
