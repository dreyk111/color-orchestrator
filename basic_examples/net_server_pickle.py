import socket, pickle

from basic_examples.some_class import Aaa

print("Server is Listening.....")
HOST = '127.0.0.1'
PORT = 65432
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(1)
    conn, addr = s.accept()
    print('Connected by', addr)
    while True:
        data = conn.recv(4096)
        if data:
            data_variable = pickle.loads(data)
            print(data_variable)
            print('Data received from client')
