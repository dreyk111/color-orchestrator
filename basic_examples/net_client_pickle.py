import socket, pickle

from basic_examples.some_class import Aaa

HOST = '127.0.0.1'
PORT = 65432
# Create a socket connection.
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))

    # Create an instance of ProcessData() to send to server.
    variable = Aaa()
    # Pickle the object and send it to the server
    data_string = pickle.dumps(variable)
    s.send(data_string)

    s.close()
    print('Data Sent to Server')
