# Подключение библиотеки PyGame
import pygame

# Инициализация PyGame
pygame.init()
# Окно игры: размер, позиция
gameScreen = pygame.display.set_mode((400, 300))
x = 100
y = 100
# Параметры окна
size = [500, 500]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Test drawings")
gameScreen.fill((0, 0, 255))
pygame.display.flip()
# Цикл игры
runGame = True  # флаг выхода из цикла игры
while runGame:
    # Отслеживание события: "закрыть окно"
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            runGame = False
# Выход из игры:
pygame.quit()
