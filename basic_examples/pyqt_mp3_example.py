import sys

from PyQt5 import QtCore, QtWidgets, QtMultimedia
from data.different_world import s

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    filename = str(s.file)
    music_file_path = QtCore.QDir.current().absoluteFilePath(filename)
    media = QtCore.QUrl.fromLocalFile(music_file_path)
    content = QtMultimedia.QMediaContent(media)
    player = QtMultimedia.QMediaPlayer()
    player.setMedia(content)
    player.play()
    sys.exit(app.exec_())
