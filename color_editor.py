import pickle
import sys
import time
from dataclasses import replace
from pathlib import Path
from typing import Literal, Union, Optional

from PyQt5 import QtCore, uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QTabWidget

# Текущий проект (устаревший механизм)
from data.my_friends import s
from models.project import Project
from models.timeline import Song
from services.devices_connector import DevicesConnector
from services.pyqt_process_all_timelines import TimelinePlayer
from services.sound_player import PygameSoundPlayer
from widgets.connection_indicator_scene import ConnectionIndicatorScene
from widgets.my_graphics_rect_item import MyQGraphicsRectItem
from widgets.patterns_editor_widget import PatternsEditorWidget
from widgets.timeline_mouse_actions import MouseActionsGroupName, TimelineMouseActionsWidget
from widgets.timelines_editor_scene import TimelinesEditorScene
from widgets.timelines_player_imitation_window import TimelinesPlayerImitationWindow, HomeLedStripGraphicsScene

CUR_PATH = Path(__file__).resolve().parent
# DATA_FILE_PATH = CUR_PATH / "data/my_friend_test2.pkl"
DATA_FILE_PATH = CUR_PATH / "data/evo_home_led_strip.pkl"  # TODO: брать из Project

# Текущий проект (новый механизм)
# CURRENT_PROJECT_PATH = CUR_PATH / r"projects\Odesza - my friends v1\Odesza - my friends v1.json"
# CURRENT_PROJECT_PATH = CUR_PATH / "projects" / "2 Phút Hơn - Pháo (KAIZ Remix) HomeLedStrip"
# CURRENT_PROJECT_PATH = CUR_PATH / r"EVO Стать мечтой HomeLedStrip"
CURRENT_PROJECT_PATH = CUR_PATH / "projects" / "Mendum-Stay with me"
CURRENT_PROJECT = Project(config_path=CURRENT_PROJECT_PATH)
# Синхронизация музыки и отображения
SONG_TIME_OFFSET = 0

sound_player = PygameSoundPlayer()  # or VLCSoundPlayer


# noinspection PyPep8Naming
class Window(QMainWindow):
    current_play_action: Union[Literal["Send timelines"], Literal["Play"], Literal["Stop"]]
    tabWidget: QTabWidget
    _first_tempo_timestamp: Optional[float] = None
    """
    Основное окно редактора. Загрузка данных проекта, соединение с устройствами. Обработка любых событий
    """
    def __init__(self):
        super().__init__()

        uic.loadUi('color_editor.ui', self)
        self.timer_id = None
        self.timer_id_repeat = None
        self.title = "Color orchestrator main"
        self.setWindowTitle(self.title)
        self.InitWindow()
        # TODO: choose imitation scene type from Project data
        self.timelinesWindow = TimelinesPlayerImitationWindow(scene=HomeLedStripGraphicsScene())
        self.timelinesWindow.show()
        self._loadProject()

        device_indicators = [scene.indicator for scene in (self.graphicsViewRaspberry1.scene(), self.graphicsViewRaspberry2.scene(), self.graphicsViewRaspberry3.scene(), self.graphicsViewRaspberry4.scene())]
        self.devices_connector = DevicesConnector(project=CURRENT_PROJECT, indicators=device_indicators)
        # self.devices_connector.connect_devices()

    def _loadProject(self):
        # init song and threads
        # Old importing:
        song = s
        # New importing:
        # TODO: delete Deprecated container Song class in favor of CurrentProject
        song = Song(bpm=CURRENT_PROJECT.config.song_params.bpm, file=CURRENT_PROJECT.music_file_path, start_time=CURRENT_PROJECT.config.song_params.start_time, timelines=CURRENT_PROJECT.timelines)
        self.cur_song = song
        self.timelines_player = TimelinePlayer(CURRENT_PROJECT.config.song_params.bpm, list(self.timelinesWindow.scene.real_rects.values()))
        self.timelines_player.start()
        self.sound_player = sound_player
        self.sound_player.init(CURRENT_PROJECT.music_file_path)
        self.lineEditTempo.setText(str(self.cur_song.bpm))
        # Init patterns editor
        self.patterns_editor_widget = PatternsEditorWidget(patterns=CURRENT_PROJECT.timeline_patterns, timeline_mouse_actions=self.timeline_mouse_actions)
        self.tabWidget.addTab(self.patterns_editor_widget, "Patterns editor")
        self.patterns_editor_widget.tabWidget.currentChanged.connect(self.changeTabHandler)

    def InitWindow(self):
        """
        Инициализация UI основного окна редактора
        """
        self.timeline_mouse_actions = TimelineMouseActionsWidget()
        self.createGraphicView()
        self.timeline_mouse_actions.set_settings(beat_width_in_pixels=self.timelines_editor_scene.BEAT_WIDTH_IN_PIXELS,
                                                                 beat_height_in_pixels=self.timelines_editor_scene.BEAT_HEIGHT_IN_PIXELS,
                                                                 timelines_offset_x=self.timelines_editor_scene.TIMELINES_OFFSET_X)
        self.timeline_mouse_actions.update_target_scene(new_scene=self.timelines_editor_scene)

        self.addObjRadioButton.clicked.connect(self._addObjRadioButtonClicked)
        self.editObjRadioButton.clicked.connect(self._editObjRadioButtonClicked)
        self.deleteObjRadioButton.clicked.connect(self._deleteObjRadioButtonClicked)
        self.setPosRadioButton.clicked.connect(self._setPosRadioButtonClicked)
        self.playPushButton.clicked.connect(self._playButtonClicked)
        self.stopPushButton.clicked.connect(self._stopEvent)
        self.pushButtonConnectRaspberry1.clicked.connect(self._pushButtonConnectRaspberry1Clicked)
        self.pushButtonConnectRaspberry2.clicked.connect(self._pushButtonConnectRaspberry2Clicked)
        self.pushButtonConnectRaspberry3.clicked.connect(self._pushButtonConnectRaspberry3Clicked)
        self.pushButtonConnectRaspberry4.clicked.connect(self._pushButtonConnectRaspberry4Clicked)
        self.pushButtonConnectRaspberryAll.clicked.connect(self._pushButtonConnectRaspberryAllClicked)
        self.pushButtonSendCurrentPattern.clicked.connect(self._pushButtonSendCurrentPatternClicked)
        self.pushButtonPlayWithRepeat.clicked.connect(self._pushButtonPlayWithRepeatClicked)

        self.lineEditLocalDelay.setText("0")
        self.lineEditRemote1Delay.setText("0")
        self.lineEditRemote2Delay.setText("0")
        self.lineEditRemote3Delay.setText("0")
        self.lineEditRemote4Delay.setText("0")
        self.graphicsViewRaspberry1.setScene(ConnectionIndicatorScene())
        self.graphicsViewRaspberry2.setScene(ConnectionIndicatorScene())
        self.graphicsViewRaspberry3.setScene(ConnectionIndicatorScene())
        self.graphicsViewRaspberry4.setScene(ConnectionIndicatorScene())
        self.show()

        self.playPushButton.setText("Send timelines")
        self.current_play_action = "Send timelines"

        self.tabWidget.currentChanged.connect(self.changeTabHandler)

    def createGraphicView(self):
        """
        Загрузка основной сцены
        """
        self.timelines_editor_scene = TimelinesEditorScene(project=CURRENT_PROJECT, timelines_file_path=None, timeline_mouse_actions=self.timeline_mouse_actions)
        # For not migrated projects:
        # self.timelines_editor_scene = TimelinesEditorScene(project=None, timelines_file_path=DATA_FILE_PATH)
        self.graphicsView.setScene(self.timelines_editor_scene)
        self.graphicsView.scale(1, 1)
        self.graphicsView.centerOn(400, 0)

    def changeTabHandler(self):
        if self.tabWidget.currentIndex() == 0:  # Main timeline
            new_scene = self.timelines_editor_scene
        elif self.tabWidget.currentIndex() == 2:   # Patterns
            new_scene = self.patterns_editor_widget.graphicViewsByTabNumber[
                self.patterns_editor_widget.tabWidget.currentIndex()].scene()
        else:
            return  # Вкладка без сцен для timeline_mouse_actions
        self.timeline_mouse_actions.update_target_scene(new_scene=new_scene)
        print("Changed target scene:", self.tabWidget.currentIndex(), self.patterns_editor_widget.tabWidget.currentIndex())

    def keyPressEvent(self, event):
        """
        Inherited handler for press button events
        """
        if event.key() == QtCore.Qt.Key_Q:
            self.deleteLater()
        elif event.key() == QtCore.Qt.Key_Plus:
            self.graphicsView.scale(1.05, 1)
        elif event.key() == QtCore.Qt.Key_Minus:
            self.graphicsView.scale(0.95, 1)
        elif event.key() == QtCore.Qt.Key_A:
            self.addObjRadioButton.click()
        elif event.key() == QtCore.Qt.Key_E:
            self.editObjRadioButton.click()
        elif event.key() == QtCore.Qt.Key_D:
            self.deleteObjRadioButton.click()
        elif event.key() == QtCore.Qt.Key_P:
            self.setPosRadioButton.click()
        # Patterns activation:
        elif event.key() == QtCore.Qt.Key_0:
            self.devices_connector.send_start_pattern(0)
        elif event.key() == QtCore.Qt.Key_1:
            self.devices_connector.send_start_pattern(1)
        elif event.key() == QtCore.Qt.Key_2:
            self.devices_connector.send_start_pattern(2)
        elif event.key() == QtCore.Qt.Key_3:
            self.devices_connector.send_start_pattern(3)
        elif event.key() == QtCore.Qt.Key_4:
            self.devices_connector.send_start_pattern(4)
        elif event.key() == QtCore.Qt.Key_5:
            self.devices_connector.send_start_pattern(5)
        elif event.key() == QtCore.Qt.Key_6:
            self.devices_connector.send_start_pattern(6)
        elif event.key() == QtCore.Qt.Key_7:
            self.devices_connector.send_start_pattern(7)
        elif event.key() == QtCore.Qt.Key_8:
            self.devices_connector.send_start_pattern(8)
        elif event.key() == QtCore.Qt.Key_9:
            self.devices_connector.send_start_pattern(9)
        elif event.key() == QtCore.Qt.Key_Z:
            self._pushButtonDefineTempoClicked()
        elif int(event.modifiers()) == QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_C:
                self.timeline_mouse_actions.buffer = [item for item in self.timeline_mouse_actions.scene.selectedItems() if
                                      isinstance(item, MyQGraphicsRectItem)]
            elif event.key() == QtCore.Qt.Key_V:
                self.timeline_mouse_actions.scene.pasteElemsFromBufferToCursor()
        event.accept()

    def resizeEvent(self, event):
        print("resized!", self.width())
        # self.graphicsView.setSceneRect(self.graphicsView.sceneRect().x(), self.graphicsView.sceneRect().y(), self.width(), self.graphicsView.sceneRect().height())
        event.accept()

    # Radiobutton handlers
    def _addObjRadioButtonClicked(self):
        self.timeline_mouse_actions.scene.timeline_mouse_actions.current_actions_name = MouseActionsGroupName.DRAW_TIMELINE

    def _editObjRadioButtonClicked(self):
        self.timeline_mouse_actions.scene.timeline_mouse_actions.current_actions_name = MouseActionsGroupName.SELECT

    def _deleteObjRadioButtonClicked(self):
        self.timeline_mouse_actions.scene.timeline_mouse_actions.current_actions_name = MouseActionsGroupName.DELETE

    def _setPosRadioButtonClicked(self):
        self.timeline_mouse_actions.scene.timeline_mouse_actions.current_actions_name = MouseActionsGroupName.SET_PLAYER_CURSOR

    def _pushButtonConnectRaspberry1Clicked(self):
        self.devices_connector.disconnect_device(0)
        self.devices_connector.connect_to_device(0)

    def _pushButtonConnectRaspberry2Clicked(self):
        self.devices_connector.disconnect_device(1)
        self.devices_connector.connect_to_device(1)

    def _pushButtonConnectRaspberry3Clicked(self):
        self.devices_connector.disconnect_device(2)
        self.devices_connector.connect_to_device(2)

    def _pushButtonConnectRaspberry4Clicked(self):
        self.devices_connector.disconnect_device(3)
        self.devices_connector.connect_to_device(3)

    def _pushButtonConnectRaspberryAllClicked(self):
        self.devices_connector.connect_devices()

    def _pushButtonSendCurrentPatternClicked(self):
        pass

    def _pushButtonDefineTempoClicked(self):
        if self._first_tempo_timestamp:
            second_tempo_timestamp = time.time()
            beat_time_in_seconds = second_tempo_timestamp - self._first_tempo_timestamp
            if beat_time_in_seconds > 5:  # Забыли нажать второй раз, значит это был первый раз
                print("Detected NEW first tempo call")
                self._first_tempo_timestamp = second_tempo_timestamp
                return
            else:
                bpm = round(60 / beat_time_in_seconds)
                self.lineEditTempo.setText(str(bpm))
                self.cur_song.bpm = bpm
                CURRENT_PROJECT.config.song_params.bpm = bpm
                self._first_tempo_timestamp = None
        else:
            self._first_tempo_timestamp = time.time()

    def _pushButtonPlayWithRepeatClicked(self):
        self.timer_id_repeat = self.startTimer(int(self.lineEditLocalDelayTimeBetweenRepeat.text())*1000)
        self.devices_connector.send_start()

    def _playButtonClicked(self):
        if self.current_play_action == "Send timelines":
            self._send_timelines()
        elif self.current_play_action == "Play":
            self._start_playing()
        else:  # Stop
            self._stopEvent()

    def _start_playing(self):
        # Update timelines fro current UI
        self.cur_song.timelines = self.timelines_editor_scene.timelines
        # related to first-beat position
        start_cursor_pos = self.timelines_editor_scene.player_cursor.pos().x()
        self.last_start_cursor_pos = start_cursor_pos

        beat_time = 60 / self.cur_song.bpm  # in seconds
        additional_beat_shift = int(start_cursor_pos / self.timelines_editor_scene.BEAT_WIDTH_IN_PIXELS)
        absolute_time_in_secs = self.cur_song.start_time + additional_beat_shift * beat_time

        self.devices_connector.send_start()

        start_local_delay = float(self.lineEditLocalDelay.text())
        time.sleep(start_local_delay)

        song_start_time_in_secs = absolute_time_in_secs + SONG_TIME_OFFSET
        self.sound_player.start_playing(position_in_secs=song_start_time_in_secs)
        self.time_start_position = time.time() - song_start_time_in_secs
        self.timer_id = self.startTimer(20)

        # начинаем проигрывать цветомузыку
        self.timelines_player.play_timelines(timelines=self.cur_song.timelines, additional_beat_shift=additional_beat_shift)
        self.playPushButton.setText("Stop")
        self.current_play_action = "Stop"

    def _send_timelines(self):
        self._dump_timelines_to_file()
        # Update timelines fro current UI
        self.cur_song.timelines = self.timelines_editor_scene.timelines
        # related to first-beat position
        start_cursor_pos = self.timelines_editor_scene.player_cursor.pos().x()
        self.last_start_cursor_pos = start_cursor_pos

        additional_beat_shift = int(start_cursor_pos / self.timelines_editor_scene.BEAT_WIDTH_IN_PIXELS)

        remote_delays = {0: float(self.lineEditRemote1Delay.text()), 1: float(self.lineEditRemote2Delay.text()), 2: float(self.lineEditRemote3Delay.text()), 3: float(self.lineEditRemote4Delay.text())}
        timelines_by_device_number = {0: list(reversed(self.cur_song.timelines[:4])), 1: list(reversed(self.cur_song.timelines[4:8])), 2: list(reversed(self.cur_song.timelines[8:12])), 3: list(reversed(self.cur_song.timelines[12:]))}  # TODO: брать из конфига
        patterns = CURRENT_PROJECT.timeline_patterns
        patterns_by_device_number = {}
        patterns_by_device_number[0] = [replace(pattern, timelines=list(reversed(pattern.timelines[:4]))) for pattern in patterns]
        patterns_by_device_number[1] = [replace(pattern, timelines=list(reversed(pattern.timelines[4:8]))) for pattern in patterns]
        patterns_by_device_number[2] = [replace(pattern, timelines=list(reversed(pattern.timelines[8:12]))) for pattern in patterns]
        patterns_by_device_number[3] = [replace(pattern, timelines=list(reversed(pattern.timelines[12:16]))) for pattern in patterns]

        self.devices_connector.send_play_timelines(remote_delays, timelines_by_device_number, additional_beat_shift=additional_beat_shift, patterns_by_device_number=patterns_by_device_number)
        self.playPushButton.setText("Play")
        self.current_play_action = "Play"

    def _stopEvent(self):
        self.sound_player.stop_playing()
        if self.timer_id:
            self.killTimer(self.timer_id)
        if self.timer_id_repeat:
            self.killTimer(self.timer_id_repeat)
        # останавливаем проигрывание таймлайнов
        self.timelines_player.stop_flag = True
        #  возвращаемся на начало проигрываемого куска
        pos = self.last_start_cursor_pos
        self.timelines_editor_scene.player_cursor.setPos(pos, 0)
        self.devices_connector.send_stop()
        self.playPushButton.setText("Send timelines")
        self.current_play_action = "Send timelines"

    def timerEvent(self, timer):
        """
        Обработка событий таймеров данного окна
        """
        if timer.timerId() == self.timer_id:
            self._setPlayerCursorToPreviousPosition()
        elif timer.timerId() == self.timer_id_repeat:
            self.devices_connector.send_stop()
            time.sleep(0.1)
            self.devices_connector.send_start()

    def _setPlayerCursorToPreviousPosition(self):
        pos_in_secs = time.time() - self.time_start_position
        beat_time = 60 / self.cur_song.bpm  # in seconds
        pos = ((pos_in_secs - self.cur_song.start_time) / beat_time) * self.timelines_editor_scene.BEAT_WIDTH_IN_PIXELS
        self.timelines_editor_scene.player_cursor.setPos(pos, 0)
        self.graphicsView.centerOn(pos + 400, 0)

    def closeEvent(self, *args, **kwargs):
        """
        Inherited handler for event of exit this window
        """
        self._dump_timelines_to_file()
        self.devices_connector.disconnect_devices()
        self.timelines_player.exit_flag = True
        self.timelinesWindow.close()
        super().closeEvent(*args, **kwargs)

    def _dump_timelines_to_file(self):
        timelines = self.timelines_editor_scene.exportTimelines()
        # Deprecated format
        with open(DATA_FILE_PATH, "wb") as file:
            pickle.dump(timelines, file, 2)
        # New format: saving file to projects (JSON) format, used also for migration from old to new format
        CURRENT_PROJECT.timelines = timelines
        CURRENT_PROJECT.timeline_patterns = self.patterns_editor_widget.export_patterns()
        CURRENT_PROJECT.dump_project_to_folder()


if __name__ == "__main__":
    App = QApplication(sys.argv)
    window = Window()
    sys.exit(App.exec())
