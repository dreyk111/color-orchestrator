from typing import List

from PyQt5.QtCore import QRectF
from PyQt5.QtGui import QColor

from models.timeline import CheckpointSettings, HSVColor, Mode, Timeline, Checkpoint
from widgets.my_graphics_rect_item import MyQGraphicsRectItem


class TimelinesToEditorWidgetAdapter:
    def __init__(self, timelines: List[Timeline], TIMELINES_OFFSET_X: int, BEAT_WIDTH_IN_PIXELS: int,
                 BEAT_HEIGHT_IN_PIXELS: int):
        self._timelines = timelines
        self.TIMELINES_OFFSET_X = TIMELINES_OFFSET_X
        self.BEAT_WIDTH_IN_PIXELS = BEAT_WIDTH_IN_PIXELS
        self.BEAT_HEIGHT_IN_PIXELS = BEAT_HEIGHT_IN_PIXELS

    def import_widget_rects_from_timelines(self, points: List[Checkpoint], y: int) -> List[MyQGraphicsRectItem]:
        qrects = []
        cur_x = self.TIMELINES_OFFSET_X  # отступ слева
        for point in points:
            width_in_pixels = point.beats * self.BEAT_WIDTH_IN_PIXELS
            if point.level == 0:
                cur_x += width_in_pixels
            elif point.level == 100:
                qrect = MyQGraphicsRectItem()
                if point.settings:
                    settings = point.settings
                    color = QColor.fromHsv(settings.color.h, settings.color.s, settings.color.v)
                else:
                    settings = CheckpointSettings(color=HSVColor(h=point.color_hue, s=255, v=255),
                                                  mode=Mode.CONSTANT)
                    color = QColor.fromHsl(point.color_hue, 255, 128)
                qrect.setMyParams(rect=QRectF(cur_x, y, width_in_pixels, self.BEAT_HEIGHT_IN_PIXELS),
                                  brush_color=color, checkpoint_settings=settings)
                qrects.append(qrect)
                cur_x += width_in_pixels
        return qrects

    def export_timelines_from_widget(self, rect_objects: List[MyQGraphicsRectItem]) -> List[Timeline]:
        if rect_objects:
            overall_last_rect = max(rect_objects, key=lambda r: r.rect().x() + r.rect().width())
            overall_last_x_coord = overall_last_rect.rect().x() + overall_last_rect.rect().width()
        else:
            overall_last_x_coord = None
        for number, timeline in enumerate(self._timelines):
            expected_y = number * self.BEAT_HEIGHT_IN_PIXELS
            rects_of_timeline = [r for r in rect_objects if r.rect().y() == expected_y]
            # Updating only checkpoints data
            timeline.checkpoints = self._timeline_points_by_rects(rects_of_timeline, overall_last_x_coord)
        return self._timelines

    def _timeline_points_by_rects(self, rects: List[MyQGraphicsRectItem], overall_last_x_coord: int) -> List[
        Checkpoint]:
        points = []
        rects_of_timeline = sorted(rects, key=lambda r: r.rect().x())
        rects_of_timeline = [r for r in rects_of_timeline if r.rect().width()]
        if rects_of_timeline:
            first_rect_start_x = rects_of_timeline[0].rect().x()
            self._add_empty_segment(points, start_x=self.TIMELINES_OFFSET_X, end_x=first_rect_start_x)
            for number, qrect in enumerate(rects_of_timeline):
                self._add_segment(points, qrect)
                current_qrect_end_x = qrect.rect().x() + qrect.rect().width()
                if number == len(rects_of_timeline) - 1:  # last rect
                    self._add_empty_segment(points, start_x=current_qrect_end_x, end_x=overall_last_x_coord)
                else:
                    next_qrect = rects_of_timeline[number + 1]
                    next_rect_start_x = next_qrect.rect().x()
                    self._add_empty_segment(points, start_x=current_qrect_end_x, end_x=next_rect_start_x)
        else:
            self._add_empty_segment(points, start_x=self.TIMELINES_OFFSET_X,
                                    end_x=self.TIMELINES_OFFSET_X + self.BEAT_WIDTH_IN_PIXELS)
        return points

    def _add_segment(self, points: list, qrect):
        rect_width_in_beats = qrect.rect().width() // self.BEAT_WIDTH_IN_PIXELS
        color_hue = qrect.brush().color().getHsl()[0]
        points.append(
            Checkpoint(level=100, beats=rect_width_in_beats, color_hue=color_hue, settings=qrect.checkpoint_settings))

    def _add_empty_segment(self, points: list, start_x: float, end_x: float):
        width_in_beats_to_first_point = (end_x - start_x) // self.BEAT_WIDTH_IN_PIXELS
        if width_in_beats_to_first_point:
            points.append(Checkpoint(level=0, beats=width_in_beats_to_first_point,
                                     settings=CheckpointSettings(color=HSVColor(0, 0, 0), mode=Mode.CONSTANT)))
