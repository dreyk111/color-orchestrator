from dataclasses import dataclass
from threading import Thread
from time import sleep

from PyQt5.QtWidgets import QGraphicsEllipseItem
from PyQt5 import QtCore

from models.project import Project
import socket

from typing import Any, Dict, List, Optional

from models.project_config import RaspberryDeviceConfig
from models.raspberry_run_config import RunRaspberryConfig
from models.timeline import Timeline, TimelinePattern


@dataclass
class DeviceConnection:
    socket: socket
    connection: Any


class DevicesConnector:
    HEALTHCHECK_SYMBOL = "^"
    START_COMMAND = "~"

    def __init__(self, project: Project, indicators: List[QGraphicsEllipseItem]):
        self.connections_to_devices_by_number: Dict[int, DeviceConnection] = {}
        self._project = project
        self._config_by_device_number: Dict[int, RaspberryDeviceConfig] = {
            device_number: device_config
            for device_number, device_config in enumerate(self._project.config.devices)
        }
        self.indicators_by_device_number = {index: indicator for index, indicator in enumerate(indicators)}
        self.healthcheck_thread = Thread(target=self.health_check_connections, name="HealthCheck connections")
        self.healthcheck_thread.start()
        self.health_check_enabled = True

    def connect_devices(self):
        connecting_threads = []
        for device_number, device_config in self._config_by_device_number.items():
            # if device_config.run:
            connecting_thread = self.connect_to_device(device_number)
            connecting_threads.append(connecting_thread)
        # for thread in connecting_threads:
        #     thread.join()

    def disconnect_devices(self):
        for (
            device_number,
            device_connection,
        ) in self.connections_to_devices_by_number.items():
            device_connection.connection.send("stop".encode("utf-8"))
            device_connection.connection.close()
            device_connection.socket.close()
            del self.connections_to_devices_by_number[device_number]
            if not self.connections_to_devices_by_number:
                break

    def send_play_timelines(
        self,
        remote_delays_by_device_number: Dict[int, float],
        timelines_by_device_number: Dict[int, List[Timeline]],
        patterns_by_device_number: Dict[int, List[TimelinePattern]],
        additional_beat_shift: int,
    ):
        self.health_check_enabled = False
        for (
            device_number,
            device_connection,
        ) in self.connections_to_devices_by_number.items():
            self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.yellow)
            remote_delay = remote_delays_by_device_number[device_number]
            config_to_send = RunRaspberryConfig(
                bpm=self._project.config.song_params.bpm,
                start_time=remote_delay,
                ports=self._project.config.devices[device_number].device_ports,
                timelines=timelines_by_device_number[device_number],
                patterns=patterns_by_device_number[device_number],
                additional_beat_shift=additional_beat_shift,
                led_strip=self._project.config.devices[device_number].led_strip,
            )
            data_string = config_to_send.to_json()
            device_connection.connection.send(data_string.encode("utf-8"))
            self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.green)
            self.health_check_enabled = True

    def send_start(self):
        for (
            device_number,
            device_connection,
        ) in self.connections_to_devices_by_number.items():
            device_connection.connection.send(self.START_COMMAND.encode("utf-8"))
            self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.darkGreen)

    def send_start_pattern(self, pattern_number: int):
        for (
            device_number,
            device_connection,
        ) in self.connections_to_devices_by_number.items():
            device_connection.connection.send(str(pattern_number).encode("utf-8"))
            self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.darkGreen)

    def send_tempo(self, bpm: int):
        for (
            device_number,
            device_connection,
        ) in self.connections_to_devices_by_number.items():
            device_connection.connection.send(f"t{bpm}".encode("utf-8"))
            self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.darkGreen)

    def send_stop(self):
        for (
            device_number,
            device_connection,
        ) in self.connections_to_devices_by_number.items():
            device_connection.connection.send("stop".encode("utf-8"))
            self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.darkGreen)

    def connect_to_device(self, device_number: int) -> Optional[Thread]:
        if device_number in self.connections_to_devices_by_number:
            print(f"Device #{device_number} already connected!")
            return
        config = self._config_by_device_number[device_number]
        print("Start connecting!")
        self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.yellow)
        connect_socket_thread = Thread(target=self.connect_socket, name=f"Connect device #{device_number} socket", args=(device_number, config))
        connect_socket_thread.start()
        return connect_socket_thread

    def disconnect_device(self, device_number: int):
        if device_number not in self.connections_to_devices_by_number:
            print(f"Device #{device_number} already deleted!")
            return
        device_connection = self.connections_to_devices_by_number[device_number]
        try:
            device_connection.connection.send("stop".encode("utf-8"))
            device_connection.connection.close()
            device_connection.socket.close()
        except Exception as e:
            print(e)
        del self.connections_to_devices_by_number[device_number]
        self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.black)

    @classmethod
    def search_device_ips(cls):
        """
        Working for Windows
        """
        BASE_IP = "192.168.1."

        import subprocess

        # this for loop depends on ho wlong you are willing to wait. I am
        for i in range(100):  # look for up to first 100 ips
            command = [
                "ping",
                "-n",
                "1",
                "-w",
                "150",
                BASE_IP + str(i),
            ]  # increment the device names
            subprocess.call(command)  # ping the devices to update data before  "arp -a'

        arpa = subprocess.check_output(("arp", "-a")).decode(
            "cp866"
        )  # call 'arp -a' and get results

        # I count lines that contain 192.168, but not ['192.168.0.1 ','192.168.0.255']
        # because those are the router and broadcast gateway. Note that the machine
        # you are running the code from will get counted because it will be in the
        # first line "Interface: 192.168.0.10 --- 0x4" in my case
        devices = [
            x
            for x in arpa.split("\r\n")
            if "192.168" in x
            and all(y not in x for y in ["192.168.1.2", "192.168.1.255"])
        ]
        print("\n".join(devices))

    def health_check_connections(self):
        while True:
            for device_number in list(self.connections_to_devices_by_number):
                device_connection = self.connections_to_devices_by_number[device_number]
                if self.health_check_enabled:
                    try:
                        device_connection.connection.send(self.HEALTHCHECK_SYMBOL.encode("utf-8"))
                    except socket.error:
                        self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.red)
                        try:
                            device_connection.connection.close()
                            device_connection.socket.close()
                        except Exception as e:
                            print(e)
                        del self.connections_to_devices_by_number[device_number]
            sleep(1)

    def connect_socket(self, device_number: int, config: RaspberryDeviceConfig):
        cur_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print(f"Trying to connect device #{device_number} ({config.name}) on port {config.network_port}")
        cur_socket.bind(("", config.network_port))
        cur_socket.listen()
        cur_connection, addr = cur_socket.accept()
        print(f"Device #{device_number} ({config.name}) Connected by", addr)
        self.connections_to_devices_by_number[device_number] = DeviceConnection(
            cur_socket, cur_connection
        )
        self.indicators_by_device_number[device_number].setBrush(QtCore.Qt.darkGreen)


if __name__ == "__main__":
    DevicesConnector.search_device_ips()
