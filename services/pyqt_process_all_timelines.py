from threading import Thread

from PyQt5.QtGui import QColor

from common.services.base_timelines_player import TimelineBasePlayer

SNAP_PER_SEC = 500


class TimelinePlayer(TimelineBasePlayer):
    def __init__(self, bpm: int, rects: list):
        Thread.__init__(self)
        self.bpm = bpm
        self.rects = rects
        self.stop_flag = True
        self.exit_flag = False

    def set_color(self, timeline_index: int, hue: int, saturation: int, lightness: int):
        self.rects[timeline_index].setBrush(QColor.fromHsl(hue, saturation, lightness))
