# import os
# os.add_dll_directory(os.getcwd())
#
# import vlc
from pathlib import Path

import pygame


class SoundPlayer:
    def init(self, song_file: Path):
        pass

    def start_playing(self, position_in_secs: int):
        pass

    def stop_playing(self):
        pass


class VLCSoundPlayer(SoundPlayer):
    def init(self, song_file: Path):
        self.instance = vlc.Instance()
        self.song_file = song_file

    def start_playing(self, position_in_secs: int):
        self.mediaplayer = self.instance.media_player_new()
        self.media = self.instance.media_new(self.song_file)
        self.media.parse()
        self.mediaplayer.set_media(self.media)
        self.mediaplayer.play()
        self.mediaplayer.set_time(int(position_in_secs * 1000))

    def stop_playing(self):
        self.mediaplayer.stop()


class PygameSoundPlayer(SoundPlayer):
    def _getmixerargs(self):
        pygame.mixer.init()
        freq, size, chan = pygame.mixer.get_init()
        return freq, size, chan

    def _initMixer(self):
        BUFFER = 3072  # audio buffer size, number of samples since pygame 1.8.
        FREQ, SIZE, CHAN = self._getmixerargs()
        pygame.mixer.init(FREQ, SIZE, CHAN, BUFFER)

    def init(self, song_file: Path):
        self._initMixer()
        self.song_file = song_file

    def start_playing(self, position_in_secs: float):
        pygame.init()
        pygame.mixer.init()
        pygame.mixer.music.set_volume(0.1)
        pygame.mixer.music.load(str(self.song_file))
        pygame.mixer.music.play(start=position_in_secs)

    def stop_playing(self):
        pygame.mixer.music.stop()
        pygame.mixer.init()
        pygame.mixer.music.load(str(self.song_file))
