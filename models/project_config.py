from dataclasses import dataclass
from typing import List, Optional

from dataclasses_json import dataclass_json

from models.raspberry_run_config import StripRaspberryConfig


@dataclass
class RaspberryDeviceConfig:
    run: bool
    device_ports: List[int]
    network_port: int  # Port to listen on (non-privileged ports are > 1023). Host = 127.0.0.1
    name: str = "Untitled device"
    led_strip: Optional[StripRaspberryConfig] = None


@dataclass
class SongParams:
    start_time: float  # in seconds
    bpm: int  # beats per second
    duration: float  # in seconds


@dataclass
class ImitationSettings:
    pass


@dataclass_json
@dataclass
class ProjectConfig:
    devices: List[RaspberryDeviceConfig]
    song_params: SongParams
    imitation_settings: ImitationSettings
