from dataclasses import dataclass
from typing import List, Optional

from dataclasses_json import dataclass_json

from models.timeline import Timeline, TimelinePattern


@dataclass_json
@dataclass
class StripRaspberryConfig:
    parts_number: int
    len_of_part: int
    parts_margin: int
    led_pin: int
    led_brightness: int # from 0 to 255
    snap_per_sec: int = 150


@dataclass_json
@dataclass
class RunRaspberryConfig:
    bpm: int
    start_time: float
    ports: List[int]
    timelines: List[Timeline]
    patterns: List[TimelinePattern]
    additional_beat_shift: int
    led_strip: Optional[StripRaspberryConfig]
