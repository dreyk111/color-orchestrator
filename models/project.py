from dataclasses import replace
from pathlib import Path
from typing import Optional

from models.project_config import ProjectConfig, RaspberryDeviceConfig, SongParams, ImitationSettings
from models.timeline import Timeline, TimelinePattern, CheckpointSettings, HSVColor, Mode
from widgets.timelines_player_imitation_window import HomeLedStripGraphicsScene


DEFAULT_PATTERN_NAMES = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p"]


class Project:
    """
    Название проекта совпадает с названием папки и config filename
    Данный класс - адаптер к файлам проекта (всем данным)
    """
    timelines_filename = "timelines.json"
    music_filename = "music.mp3"
    config_filename = "config.json"
    timeline_patterns_filename = "timeline_patterns.json"

    def __init__(self, config_path: Path):
        self.folder = config_path.resolve()
        self._load_config()
        self._load_timelines()
        self._load_timeline_patterns()

    def dump_project_to_folder(self, project_folder_path: Optional[Path] = None, name: Optional[str] = None):
        if not name:
            name = self.folder.name
        if not project_folder_path:
            project_folder = self.folder
        else:
            # make folder for new project
            project_folder = project_folder_path / name
            project_folder.mkdir(exist_ok=True, parents=True)
        with open(project_folder / self.config_filename, "w", encoding="utf8") as f:
            f.writelines(self.config.to_json(sort_keys=True, indent=4))
        with open(project_folder / self.timelines_filename, "w", encoding="utf8") as f:
            f.writelines(Timeline.schema().dumps(self.timelines, many=True, ensure_ascii=False, sort_keys=True, indent=4))
        with open(project_folder / self.timeline_patterns_filename, "w", encoding="utf8") as f:
            f.writelines(TimelinePattern.schema().dumps(self.timeline_patterns, many=True, ensure_ascii=False, sort_keys=True, indent=4))

    @property
    def music_file_path(self) -> Path:
        return self.folder / self.music_filename

    def _load_config(self):
        config_file_path = self.folder / self.config_filename
        if config_file_path.exists():
            with open(config_file_path, "r", encoding="utf8") as f:
                config_json = f.read()
        else:
            config_json = None
        if not config_json:
            self.config = ProjectConfig(
                devices=[RaspberryDeviceConfig(run=False, device_ports=[21, 26, 20, 19, 16, 13], network_port=65431)],
                song_params=SongParams(start_time=0.8, bpm=149, duration=165),
                imitation_settings=ImitationSettings())
        else:
            self.config = ProjectConfig.from_json(config_json)

    def _load_timelines(self):
        timelines_file_path = self.folder / self.timelines_filename
        if timelines_file_path.exists():
            with open(timelines_file_path, "r", encoding="utf8") as f:
                timelines_json = f.read()
        else:
            timelines_json = None
        if not timelines_json:
            self.timelines = [Timeline(name, []) for name in HomeLedStripGraphicsScene.rects_with_positions.keys()]
        else:
            timelines = Timeline.schema().loads(timelines_json, many=True)
            new_timelines = []
            for timeline in timelines:
                new_checkpoints = []
                for checkpoint in timeline.checkpoints:
                    if not checkpoint.settings:
                        settings = CheckpointSettings(color=HSVColor(checkpoint.color_hue, 255, 255), mode=Mode.CONSTANT)
                    else:
                        settings = checkpoint.settings
                    new_checkpoint = replace(checkpoint, settings=settings)
                    new_checkpoints.append(new_checkpoint)
                new_timeline = replace(timeline, checkpoints=new_checkpoints)
                new_timelines.append(new_timeline)
            self.timelines = new_timelines

    def _load_timeline_patterns(self):
        file_path = self.folder / self.timeline_patterns_filename
        if file_path.exists():
            with open(file_path, "r", encoding="utf8") as f:
                json_data = f.read()
        else:
            json_data = None
        if not json_data:
            init_timelines = [Timeline(name, []) for name in HomeLedStripGraphicsScene.rects_with_positions.keys()]
            self.timeline_patterns = [TimelinePattern(name, init_timelines) for name in DEFAULT_PATTERN_NAMES]
        else:
            self.timeline_patterns = TimelinePattern.schema().loads(json_data, many=True)
