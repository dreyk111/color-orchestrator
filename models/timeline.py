"""
Модели должны быть синхронизированы с raspberry_color_client.models, которые используются также на устройствах Raspberry
"""
from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from typing import List, Optional

from dataclasses_json import dataclass_json


class Mode(Enum):
    CONSTANT = "constant"  # По достижению этого момента времени просто установить новое значение
    LINEAR = "linear"  # К данной точке необходимо прийти постепенно градиентом по отношению к предыдущей точке
    RUNNING_BEAM_UP = "RUNNING_BEAM_UP"
    RUNNING_BEAM_DOWN = "RUNNING_BEAM_DOWN"
    CHASE = "CHASE"
    FADE_IN = "FADE_IN"
    FADE_OUT = "FADE_OUT"


@dataclass_json
@dataclass
class HSVColor:
    h: int  # from o to 359
    s: int  # from 0 to 255
    v: int  # from 0 to 255


@dataclass_json
@dataclass
class CheckpointSettings:  # все настройки кроме длительности теперь тут
    color: HSVColor
    mode: Mode
    repeat: bool = False
    speed_in_beats: float = 1


@dataclass_json
@dataclass
class Checkpoint:
    """
    Одно действие, которое необходимо сделать в определённый момент времени
    """
    beats: float  # time from start in beats
    level: int  # 0-100%
    mode: Mode = Mode.CONSTANT
    color_hue: Optional[int] = 0
    settings: Optional[CheckpointSettings] = None


@dataclass_json
@dataclass
class Timeline:
    """
    Класс с данными, в какой момент времени что необходимо сделать на этом канале
    """
    name: str
    checkpoints: List[Checkpoint] = field(default_factory=list)


@dataclass
class Song:  # deprecated in favor of Project
    file: Path
    start_time: float  # in seconds  # deprecated
    bpm: int  # beats per second  # deprecated
    timelines: List[Timeline]  # main data


@dataclass_json
@dataclass
class TimelinePattern:
    name: str
    timelines: List[Timeline]
