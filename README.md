Проект для создания световых шоу для одного или нескольких костюмов.

На данный момент поддерживаются ровно два костюма на Raspberry Pi.

Данная версия запуска звука отлично работает на платформе Windows, на платформе Linux звук сдвигается, если запускать таймлайн не с начала.

Требуемая версия Python: 3.7+

### Запуск редактора/сервера:
```
python3 -m venv env
source venv/bin/activate
pip install -r requirements.txt
```
Актуальный запуск на Windows:
```
python color_editor.py
```
Актуальный запуск на Linux:
```
В файле color_editor.py установить sound_player = VLCSoundPlayer()
python color_editor.py
```
### Запуск на Raspberry:
* зайти в папку raspberry_color_client
```
cd raspberry_color_client
```
* В файле run_raspberry_client.py указать актуальный IP адрес сервера
* Проверить, что порт данного клиента совпадает с ожидаемым на сервере
* После запуска сервера, запустить клиент
```
sudo python3 run_raspberry_client.py
```

### Установка и настройка Raspberry:
* установить образ Raspbian на карту памяти http://wiki.amperka.ru/articles:write-rpi-sd 
* установить Git
* Скачать данный проект:
```
git clone git@bitbucket.org:dreyk111/color-orchestrator.git
```
Добавить клиент в автозапуск
```
sudo cp raspberry_color_client/raspberry-color-client.service /etc/systemd/system
sudo chmod +x raspberry_color_client/start-service.sh
sudo systemctl daemon-reload
sudo systemctl enable raspberry-color-client
sudo systemctl start raspberry-color-client

systemctl status raspberry-color-client
journalctl -u raspberry-color-client
```

Как перезапустить клиент удалённо, если он завис:
* подключиться через SSH
* Перезапустить службу:
```
sudo systemctl restart raspberry-color-client
```