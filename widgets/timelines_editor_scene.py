import pickle
from pathlib import Path
from typing import Optional, List

from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtCore import QRectF
from PyQt5.QtGui import QPen, QFont, QColor

from models.project import Project
from models.timeline import CheckpointSettings, HSVColor, Mode, Timeline
from services.timelines_to_editor_widget_adapter import TimelinesToEditorWidgetAdapter
from widgets.my_graphics_rect_item import MyQGraphicsRectItem
from widgets.timeline_mouse_actions import TimelineMouseActionsWidget

DEFAULT_TIMELINES_LABELS = ["Левая рука", "Правая рука", "Левая нога", "Правая нога", "Туловище", "Пояс"]
DEFAULT_ACTORS = ["Ульяна", "Артём"]


# noinspection PyPep8Naming
class TimelinesEditorScene(QtWidgets.QGraphicsScene):
    default_timeline_labels = [f"{timeline} {actor}" for actor in DEFAULT_ACTORS for timeline in
                               DEFAULT_TIMELINES_LABELS]
    # Настройки отображения (масштаб):
    BEAT_WIDTH_IN_PIXELS = 16
    BEAT_HEIGHT_IN_PIXELS = 40
    OFFSET_X_BETWEEN_TIMELINE_LABELS = 300
    TIMELINES_OFFSET_X = 220
    TIMELINES_ADAPTER_CLS = TimelinesToEditorWidgetAdapter

    def __init__(self, project: Optional[Project], timelines_file_path: Optional[Path], timeline_mouse_actions: TimelineMouseActionsWidget, timelines: Optional[List[Timeline]] = None, parent=None):
        super(TimelinesEditorScene, self).__init__(parent)
        if timelines:
            self.timelines = timelines
            self.MAX_BEATS_IN_SONG = 1000
        elif project:
            self.MAX_BEATS_IN_SONG = project.config.song_params.duration * project.config.song_params.bpm / 60
            self.timelines = project.timelines
        else:
            self.MAX_BEATS_IN_SONG = 1000
            # загружаем timelines из файла, если файл не пуст
            try:
                with open(timelines_file_path, "rb") as file:
                    timelines = pickle.load(file)
                    if timelines:
                        self.timelines = timelines
                    else:
                        self.timelines = [Timeline(name=label, checkpoints=[]) for label in
                                          self.default_timeline_labels]
            except FileNotFoundError:
                self.timelines = [Timeline(name=label, checkpoints=[]) for label in self.default_timeline_labels]
        self.MAX_LENGTH_X_IN_PIXELS = round(self.MAX_BEATS_IN_SONG * self.BEAT_WIDTH_IN_PIXELS)
        self.data_adapter = TimelinesToEditorWidgetAdapter(timelines=self.timelines,
                                                           TIMELINES_OFFSET_X=self.TIMELINES_OFFSET_X,
                                                           BEAT_WIDTH_IN_PIXELS=self.BEAT_WIDTH_IN_PIXELS,
                                                           BEAT_HEIGHT_IN_PIXELS=self.BEAT_HEIGHT_IN_PIXELS)
        self.showDefaultGraphics()
        self.timeline_mouse_actions = timeline_mouse_actions

    def showDefaultGraphics(self):
        self.MAX_LINE_NUMBER_X = (len(self.timelines))
        self._showGrid()
        self.player_cursor = self.addLine(self.TIMELINES_OFFSET_X, 0, self.TIMELINES_OFFSET_X,
                                          self.MAX_LINE_NUMBER_X * self.BEAT_HEIGHT_IN_PIXELS,
                                          pen=QPen(QtCore.Qt.darkGreen, 5))
        self._showLoadedTimelines()
        self._showTimelinesLabels()

    def _showTimelinesLabels(self):
        for num, text in enumerate(t.name for t in self.timelines):
            label = self.addText(text)
            label.setPos(100, num * self.BEAT_HEIGHT_IN_PIXELS + 15)
            for position_x in range(self.TIMELINES_OFFSET_X, self.MAX_LENGTH_X_IN_PIXELS,
                                    self.OFFSET_X_BETWEEN_TIMELINE_LABELS):
                myFont = QFont()
                myFont.setBold(True)
                label = self.addText(text)
                label.setFont(myFont)
                label.setPos(position_x, num * self.BEAT_HEIGHT_IN_PIXELS + 15)

    def _showGrid(self):
        for x in range(self.TIMELINES_OFFSET_X, self.MAX_LENGTH_X_IN_PIXELS, self.BEAT_WIDTH_IN_PIXELS):
            if not (x - self.TIMELINES_OFFSET_X) // self.BEAT_WIDTH_IN_PIXELS % 4:
                if not (x - self.TIMELINES_OFFSET_X) // self.BEAT_WIDTH_IN_PIXELS % 16:
                    self.addLine(x, -5, x, self.MAX_LINE_NUMBER_X * self.BEAT_HEIGHT_IN_PIXELS,
                                 pen=QPen(QtCore.Qt.darkBlue, 3))
                else:
                    self.addLine(x, -5, x, self.MAX_LINE_NUMBER_X * self.BEAT_HEIGHT_IN_PIXELS,
                                 pen=QPen(QtCore.Qt.blue, 3))
                label = self.addText(str((x - self.TIMELINES_OFFSET_X) // self.BEAT_WIDTH_IN_PIXELS // 4))
                label.setPos(x - 10, -25)
            else:
                self.addLine(x, -5, x, self.MAX_LINE_NUMBER_X * self.BEAT_HEIGHT_IN_PIXELS, QtCore.Qt.blue)
        for y in range(0, (self.MAX_LINE_NUMBER_X + 1) * self.BEAT_HEIGHT_IN_PIXELS, self.BEAT_HEIGHT_IN_PIXELS):
            self.addLine(self.TIMELINES_OFFSET_X, y, self.MAX_LENGTH_X_IN_PIXELS, y, QtCore.Qt.blue)

    def _showLoadedTimelines(self):
        for number, timeline in enumerate(self.timelines):
            timeline_y = number * self.BEAT_HEIGHT_IN_PIXELS
            for qrect in self.data_adapter.import_widget_rects_from_timelines(timeline.checkpoints, timeline_y):
                self.addItem(qrect)
                # new_qrect = self.addMyRect(qrect.rect(), qrect.brush().color())

    def mousePressEvent(self, event):
        self.update()
        self.timeline_mouse_actions.mousePressEventHandler(event)
        self.update()
        super(TimelinesEditorScene, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        self.update()
        self.timeline_mouse_actions.mouseMoveEventHandler(event)
        if self.timeline_mouse_actions.buffer:
            self._cur_mouse_pos = event.scenePos()
        self.update()
        super(TimelinesEditorScene, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        self.timeline_mouse_actions.mouseReleaseEventHandler(event)
        self.update()
        super(TimelinesEditorScene, self).mouseReleaseEvent(event)

    def pasteElemsFromBufferToCursor(self):
        if self.timeline_mouse_actions.buffer:
            # rounded_x, rounded_y = (round(self._cur_mouse_pos.x()-self.TIMELINES_OFFSET_X / self.BEAT_WIDTH_IN_PIXELS - 0.5) * self.BEAT_WIDTH_IN_PIXELS+self.TIMELINES_OFFSET_X,
            #                        round(self._cur_mouse_pos.y() / self.BEAT_HEIGHT_IN_PIXELS - 0.5) * self.BEAT_HEIGHT_IN_PIXELS)
            rounded_x, rounded_y = round((
                                                     self._cur_mouse_pos.x() - self.TIMELINES_OFFSET_X) / self.BEAT_WIDTH_IN_PIXELS - 0.5) * self.BEAT_WIDTH_IN_PIXELS + self.TIMELINES_OFFSET_X, \
                                   round(
                                       self._cur_mouse_pos.y() / self.BEAT_HEIGHT_IN_PIXELS - 0.5) * self.BEAT_HEIGHT_IN_PIXELS
            left_rect = sorted(self.timeline_mouse_actions.buffer, key=lambda item: item.rect().x())[0].rect()
            up_rect = sorted(self.timeline_mouse_actions.buffer, key=lambda item: item.rect().y())[0].rect()
            min_x = left_rect.x()
            min_y = up_rect.y()
            for item in self.timeline_mouse_actions.buffer:
                rect = item.rect()
                new_x = rounded_x + rect.x() - min_x
                new_y = rounded_y + rect.y() - min_y
                new_rect = QRectF(new_x, new_y, rect.width(), rect.height())
                print("Passing rect from buffer at coords: ", new_x, new_y)
                self.addMyRect(new_rect, item.brush().color(), item.checkpoint_settings)
            self.update()

    def addMyRect(self, rect: QRectF, brush_color: Optional[QColor] = QColor(QtCore.Qt.red),
                  checkpoint_settings: Optional[CheckpointSettings] = None) -> MyQGraphicsRectItem:
        if not checkpoint_settings:
            checkpoint_settings = CheckpointSettings(HSVColor(h=brush_color.hue(), s=255, v=255),
                                                                mode=Mode.CONSTANT)
        graphics_rect = MyQGraphicsRectItem()
        graphics_rect.setMyParams(rect=rect, brush_color=brush_color, checkpoint_settings=checkpoint_settings)
        self.addItem(graphics_rect)
        return graphics_rect

    def _getRectItems(self) -> List[MyQGraphicsRectItem]:
        return [item for item in self.items() if isinstance(item, MyQGraphicsRectItem)]

    def exportTimelines(self) -> List[Timeline]:
        return self.data_adapter.export_timelines_from_widget(rect_objects=self._getRectItems())
