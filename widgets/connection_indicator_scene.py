from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QGraphicsEllipseItem


class ConnectionIndicatorScene(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.indicator: QGraphicsEllipseItem = self.addEllipse(0, 0, 100, 100, brush=QtCore.Qt.black)
