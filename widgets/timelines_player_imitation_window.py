from dataclasses import dataclass
from typing import Dict

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QRectF
from PyQt5.QtGui import QColor


@dataclass
class TimelineView:
    rect: QRectF
    color_hue: int  # from 0 to 360


# Interface
class TimelinesImitationGraphicsScene(QtWidgets.QGraphicsScene):
    rects_with_positions: Dict[str, TimelineView] = {}
    real_rects = {}

    def addMyRect(self, rect: QtCore.QRectF, color):
        rect_item = QtWidgets.QGraphicsRectItem()
        rect_item.setBrush(color)
        rect_item.setRect(rect)
        self.addItem(rect_item)
        return rect_item

    def _initRects(self):
        for rect_label, rect_obj in self.rects_with_positions.items():
            # self.real_rects[rect_label] = self.addMyRect(rect_obj.rect, QColor.fromHsl(rect_obj.color_hue, 140, 160))
            self.real_rects[rect_label] = self.addMyRect(rect_obj.rect, QColor.fromRgb(0, 0, 0))


class UlianaArtemGraphicsScene(TimelinesImitationGraphicsScene):
    delta_x = 400
    # TODO: make UI and save to project file instead of hard-code
    rects_with_positions = {"Левая рука 1": TimelineView(QRectF(50, 100, 100, 50), 180),
                            "Правая рука 1": TimelineView(QRectF(300, 100, 100, 50), 180),
                            "Левая нога 1": TimelineView(QRectF(150, 265, 50, 100), 180),
                            "Правая нога 1": TimelineView(QRectF(250, 265, 50, 100), 180),
                            # "Голова 1": TimelineView(QRectF(350, 200, 90, 90), 180),
                            "Туловище 1": TimelineView(QRectF(190, 40, 65, 140), 180),
                            "Пояс 1": TimelineView(QRectF(150, 200, 150, 40), 310),
                            "Левая рука 2": TimelineView(QRectF(50+delta_x, 100, 100, 50), 180),
                            "Правая рука 2": TimelineView(QRectF(300+delta_x, 100, 100, 50), 180),
                            "Левая нога 2": TimelineView(QRectF(150+delta_x, 265, 50, 100), 180),
                            "Правая нога 2": TimelineView(QRectF(250+delta_x, 265, 50, 100), 180),
                            # "Голова 2": TimelineView(QRectF(350, 200, 90, 90), 180),
                            "Туловище 2": TimelineView(QRectF(190+delta_x, 40, 70, 140), 180),
                            "Пояс 2": TimelineView(QRectF(150+delta_x, 200, 150, 40), 310)
                            }

    def __init__(self, parent=None):
        super(UlianaArtemGraphicsScene, self).__init__(QRectF(0, 0, 850, 450), parent)
        self._start = QtCore.QPointF()
        self._initRects()
        label = self.addText("Ульяна")
        label.setPos(180, 400)
        label = self.addText("Артём")
        label.setPos(180+self.delta_x, 400)


class HomeLedStripGraphicsScene(TimelinesImitationGraphicsScene):
    # TODO: make UI and save to project file instead of hard-code
    rects_with_positions = {"Лево перед 1": TimelineView(QRectF(50, 100, 60, 50), 180),
                            "Лево перед 2": TimelineView(QRectF(120, 100, 60, 50), 180),
                            "Лево перед 3": TimelineView(QRectF(190, 100, 60, 50), 180),
                            "Лево перед 4": TimelineView(QRectF(260, 100, 60, 50), 180),
                            "Право перед 1": TimelineView(QRectF(350, 100, 60, 50), 180),
                            "Право перед 2": TimelineView(QRectF(420, 100, 60, 50), 180),
                            "Право перед 3": TimelineView(QRectF(490, 100, 60, 50), 180),
                            "Право перед 4": TimelineView(QRectF(560, 100, 60, 50), 180),
                            "Лево зад 1": TimelineView(QRectF(350, 100, 60, 50), 180),
                            "Лево зад 2": TimelineView(QRectF(420, 100, 60, 50), 180),
                            "Лево зад 3": TimelineView(QRectF(490, 100, 60, 50), 180),
                            "Лево зад 4": TimelineView(QRectF(560, 100, 60, 50), 180),
                            "Право зад 1": TimelineView(QRectF(350, 100, 60, 50), 180),
                            "Право зад 2": TimelineView(QRectF(420, 100, 60, 50), 180),
                            "Право зад 3": TimelineView(QRectF(490, 100, 60, 50), 180),
                            "Право зад 4": TimelineView(QRectF(560, 100, 60, 50), 180),
                            }

    def __init__(self, parent=None):
        super(HomeLedStripGraphicsScene, self).__init__(QRectF(0, 0, 850, 450), parent)
        self._start = QtCore.QPointF()
        self._initRects()


class TimelinesPlayerImitationWindow(QtWidgets.QMainWindow):
    def __init__(self, scene: QtWidgets.QGraphicsScene, parent=None):
        super(TimelinesPlayerImitationWindow, self).__init__(parent)
        self.scene = scene
        self.view = QtWidgets.QGraphicsView(self.scene)
        self.setCentralWidget(self.view)
        self.setWindowTitle("Timelines player imitation")
