from pathlib import Path
from typing import List

from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QTabWidget, QGraphicsView

from models.timeline import TimelinePattern
from widgets.timeline_mouse_actions import TimelineMouseActionsWidget
from widgets.timelines_editor_scene import TimelinesEditorScene

CUR_PATH = Path(__file__).resolve().parent


class PatternsEditorWidget(QWidget):
    tabWidget: QTabWidget

    def __init__(self, patterns: List[TimelinePattern], timeline_mouse_actions: TimelineMouseActionsWidget, parent=None):
        super(PatternsEditorWidget, self).__init__(parent)
        uic.loadUi(CUR_PATH / 'patterns-editor.ui', self)
        self.tabWidget.removeTab(1)
        self.tabWidget.removeTab(0)
        self.graphicViewsByTabNumber = {}
        for number, pattern in enumerate(patterns):
            self.graphicViewsByTabNumber[number] = QGraphicsView(parent=self)
            scene = TimelinesEditorScene(timelines=pattern.timelines, timelines_file_path=None, project=None, timeline_mouse_actions=timeline_mouse_actions)
            self.graphicViewsByTabNumber[number].setScene(scene)
            self.graphicViewsByTabNumber[number].centerOn(400, 0)
            self.tabWidget.addTab(self.graphicViewsByTabNumber[number], pattern.name)

    def export_patterns(self) -> List[TimelinePattern]:
        patterns = []
        for tab_number in self.graphicViewsByTabNumber.keys():
            timelines = self._export_timelines_from_tab(tab_number)
            new_name = self.tabWidget.tabText(tab_number)
            pattern = TimelinePattern(timelines=timelines, name=new_name)
            patterns.append(pattern)
        return patterns

    def _export_timelines_from_tab(self, tab_number: int):
        return self.graphicViewsByTabNumber[tab_number].scene().exportTimelines()
