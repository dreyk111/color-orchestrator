from pathlib import Path
from typing import Optional, Dict

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QPointF, QRect
from PyQt5.QtCore import QRectF
from PyQt5.QtGui import QColor, QPainterPath, QPainter, QPixmap, QTransform, QBrush
from PyQt5.QtWidgets import QDialog, QGraphicsItem

from models.timeline import CheckpointSettings, Mode
from widgets.timepoint_settings_window import TimepointSettingsWindow


CUR_DIR = Path(__file__).resolve().parent


class MyQGraphicsRectItem(QGraphicsItem):
    checkpoint_settings: Optional[CheckpointSettings] = None
    icons_dir = CUR_DIR / "icons"
    pixmaps_by_mode: Dict[Mode, QPixmap] = {}

    def __init__(self, parent=None):
        super(MyQGraphicsRectItem, self).__init__(parent)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setAcceptHoverEvents(True)

    def boundingRect(self):
        return self.myrect

    def rect(self):
        return self.myrect

    def brush(self):
        return self.mybrush

    def paint(self, painter, option, widget):
        if self.checkpoint_settings:
            painter.save()
            painter.setBrush(self.mybrush)
            if self.checkpoint_settings and self.checkpoint_settings.mode in [Mode.FADE_IN, Mode.FADE_OUT]:
                self._draw_triangle(self.myrect, self.checkpoint_settings.mode, painter)
            else:
                self._draw_rect(self.myrect, self.checkpoint_settings.mode, painter)
            painter.restore()

    def setMyParams(self, rect: QRectF, brush_color: QColor, checkpoint_settings: CheckpointSettings = None):
        self.myrect = rect
        self.mybrush = QBrush(brush_color)
        self.checkpoint_settings = checkpoint_settings
        # graphics_rect.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
        flags = [QtWidgets.QGraphicsItem.ItemIsSelectable]
        for flag in flags:
            self.setFlag(flag, True)
        self.checkpoint_settings = checkpoint_settings
        self._init_icons()

    def setRect(self, rect: QRectF):
        self.myrect = rect

    # Moving item disabled
    # def moveBy(self, dx, dy):
    #     dx = dx // BEAT_WIDTH_IN_PIXELS
    #     dy = dy // 50
    #     return super(MyQGraphicsRectItem, self).moveBy(dx, dy)

    def setPos(self, pos):
        super(MyQGraphicsRectItem, self).setPos(pos)

    def mousePressEvent(self, QMouseEvent):
        if QMouseEvent.button() == Qt.RightButton:
            self.dialog_window = TimepointSettingsWindow(settings=self.checkpoint_settings)
            if self.dialog_window.exec_() == QDialog.Accepted:
                self.setAcceptedSettings()

    def setAcceptedSettings(self):
        self.checkpoint_settings = self.dialog_window.exportSettings()
        self.mybrush = QBrush(self.dialog_window._color)

    def _draw_triangle(self, rect: QRectF, mode: Mode, painter: QPainter):
        if mode == Mode.FADE_IN:
            pos_top = QPointF(rect.x()+rect.width(), rect.y())
        else:
            pos_top = QPointF(rect.x(), rect.y())
        pos_left = QPointF(rect.x(), rect.y()+rect.height())
        pos_right = QPointF(rect.x()+rect.width(), rect.y()+rect.height())

        path = QPainterPath()
        path.moveTo(pos_top)
        path.lineTo(pos_right)
        path.lineTo(pos_left)
        painter.drawPath(path)

    def _draw_rect(self, rect: QRectF, mode: Mode, painter: QPainter):
        icon_width = 15
        icon_height = 22
        painter.drawRect(rect)
        icon_rect = QRect(int(rect.x()+rect.width()-icon_width), int(rect.y()+rect.height()-icon_height), icon_width, icon_height)
        if mode in self.pixmaps_by_mode:
            painter.drawPixmap(icon_rect, self.pixmaps_by_mode[mode])

    def _init_icons(self):
        running_beam = QPixmap(str(self.icons_dir / "running-beam.png"))
        running_beam_up = running_beam.transformed(QTransform().rotate(-90))
        running_beam_down = running_beam.transformed(QTransform().rotate(+90))
        self.pixmaps_by_mode = {Mode.RUNNING_BEAM_UP: running_beam_up,
                Mode.RUNNING_BEAM_DOWN: running_beam_down, Mode.CHASE: QPixmap(str(self.icons_dir / "flicker.png"))}
