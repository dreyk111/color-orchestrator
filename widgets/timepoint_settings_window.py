import sys

from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QApplication, QColorDialog, QGraphicsRectItem

from models.timeline import CheckpointSettings, HSVColor, Mode
from utils import ROOT_DIR

UI_PATH = ROOT_DIR / "widgets" / "timepointSettings.ui"


class ViewColorScene(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.indicator: QGraphicsRectItem = self.addRect(0, 0, 120, 120, brush=QtCore.Qt.black)


class TimepointSettingsWindow(QtWidgets.QDialog):
    def __init__(self, settings: CheckpointSettings, parent=None):
        super(TimepointSettingsWindow, self).__init__()
        uic.loadUi(str(UI_PATH), self)
        self.graphicsViewColor.scene = ViewColorScene()
        self.graphicsViewColor.setScene(self.graphicsViewColor.scene)
        self.graphicsViewColor.mousePressEvent = self.getNewColor
        self.radioButtonsToModeMapping = {self.radioButtonConstant: Mode.CONSTANT,
                                          self.radioButtonRunningBeamUp: Mode.RUNNING_BEAM_UP,
                                          self.radioButtonRunningBeamDown: Mode.RUNNING_BEAM_DOWN,
                                          self.radioButtonChase: Mode.CHASE,
                                          self.radioButtonFadeIn: Mode.FADE_IN,
                                          self.radioButtonFadeOut: Mode.FADE_OUT}
        self.importSettings(settings)
        # self.show()

    def getNewColor(self, event):
        self._color = QColorDialog.getColor()
        self.graphicsViewColor.scene.indicator.setBrush(self._color)

    def importSettings(self, settings: CheckpointSettings):
        for radioButton, mode in self.radioButtonsToModeMapping.items():
            if settings.mode == mode:
                radioButton.setChecked(True)
            else:
                radioButton.setChecked(False)
        self._color = QColor.fromHsv(settings.color.h, settings.color.s, settings.color.v)
        self.graphicsViewColor.scene.indicator.setBrush(self._color)
        self.checkBoxRepeat.setChecked(settings.repeat)
        self.lineEditSpeed.setText(str(settings.speed_in_beats))

    def exportSettings(self) -> CheckpointSettings:
        result_mode = None

        for radioButton, mode in self.radioButtonsToModeMapping.items():
            if radioButton.isChecked():
                result_mode = mode
        q_hsv_color = self._color.getHsv()
        color = HSVColor(h=q_hsv_color[0], s=q_hsv_color[1], v=q_hsv_color[2])
        repeat = self.checkBoxRepeat.isChecked()
        speed = float(self.lineEditSpeed.text())
        return CheckpointSettings(color=color,
                                  mode=result_mode,
                                  repeat=repeat,
                                  speed_in_beats=speed)

    # def submitclose(self):
    #     self.accept()


def handler(a):
    print(a)


if __name__ == "__main__":
    App = QApplication(sys.argv)
    window = TimepointSettingsWindow()
    window.accepted.connect(handler, )
    sys.exit(App.exec())
