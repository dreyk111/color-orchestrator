from abc import abstractmethod
from enum import Enum
from typing import List, Dict, Optional

from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import QRectF, QPointF
from PyQt5.QtGui import QPainterPath
from PyQt5.QtWidgets import QGraphicsRectItem, QGraphicsScene

from widgets.my_graphics_rect_item import MyQGraphicsRectItem


# noinspection PyPep8Naming
class MouseActions:
    def __init__(self, scene: QGraphicsScene):
        self.scene = scene

    @abstractmethod
    def mousePressEvent(self, event):
        pass

    @abstractmethod
    def mouseMoveEvent(self, event):
        pass

    @abstractmethod
    def mouseReleaseEvent(self, event):
        pass


class MouseActionsGroupName(Enum):
    DRAW_TIMELINE = "DRAW_TIMELINE"
    SELECT = "SELECT"
    SET_PLAYER_CURSOR = "SET_PLAYER_CURSOR"
    DELETE = "DELETE"


# noinspection PyPep8Naming
class DrawTimelineMouseActions(MouseActions):
    def __init__(self, scene: QGraphicsScene, beat_width_in_pixels: int, beat_height_in_pixels: int,
                 timelines_offset_x: int):
        self._current_rect_item = None
        self._start: Optional[QPointF] = None
        self._beat_width = beat_width_in_pixels
        self._beat_height = beat_height_in_pixels
        self._offset_x = timelines_offset_x
        super().__init__(scene)

    def mousePressEvent(self, event):
        if self.scene.itemAt(event.scenePos(), QtGui.QTransform()) is None:
            cur_point = event.scenePos()
            rounded_point = QPointF(
                (round((cur_point.x() - self._offset_x) / self._beat_width) * self._beat_width) + self._offset_x,
                round(cur_point.y() / self._beat_height) * self._beat_height)
            r = QRectF(rounded_point.x(), rounded_point.y(), 4, 4)
            self._start = rounded_point
            self._current_rect_item = self.scene.addMyRect(r)

    def mouseMoveEvent(self, event):
        # FIXME: если курсор по итогу никуда не сдвинулся, добавлять точку в таймлайн не надо!
        if self._current_rect_item is not None:
            cur_point = event.scenePos()
            rounded_point = QtCore.QPointF(
                (round((cur_point.x() - self._offset_x) / self._beat_width) * self._beat_width) + self._offset_x,
                round(cur_point.y() / self._beat_height + 0.5) * self._beat_height)
            # r = QRectF(self._start, rounded_point).normalized()
            width = rounded_point.x() - self._start.x()
            height = rounded_point.y() - self._start.y()
            if width > 0 and height > 0:
                r = QRectF(self._start.x(), self._start.y(), width, height)
                self._current_rect_item.setRect(r)
            else:
                r = QRectF(self._start.x(), self._start.y(), 4, 4)
                self._current_rect_item.setRect(r)

    def mouseReleaseEvent(self, event):
        if self._current_rect_item and self._current_rect_item.rect().width() <= 4:
            self.scene.removeItem(self._current_rect_item)  # Курсор по оси X вернулся в ту же точку
        for number, qrect in enumerate(
                sorted((item for item in self.scene.items() if isinstance(item, QGraphicsRectItem)),
                       key=lambda item: item.rect().x())):
            rect = qrect.rect()
            # print(number, ":", rect.x(), rect.y(), rect.width(), rect.height())

        self._current_rect_item = None


# noinspection PyPep8Naming
class SelectionMouseActions(MouseActions):
    def __init__(self, scene: QGraphicsScene):
        self._start = None
        super().__init__(scene)

    def mousePressEvent(self, event):
        if self.scene.itemAt(event.scenePos(), QtGui.QTransform()) is None:
            cur_point = event.scenePos()
            self._start = cur_point

    def mouseMoveEvent(self, event):
        if self._start:
            cur_point = event.scenePos()
            path = QPainterPath()
            path.addRect(QRectF(self._start, cur_point))
            self.scene.setSelectionArea(path)

    def mouseReleaseEvent(self, event):
        self._start = None


class SetPlayerCursorActions(MouseActions):
    def __init__(self, scene: QGraphicsScene, beat_width_in_pixels: int, timelines_offset_x: int):
        self.scene = scene
        self._beat_width = beat_width_in_pixels
        self._offset_x = timelines_offset_x
        super().__init__(scene)

    def mousePressEvent(self, event):
        cur_point = event.scenePos()
        rounded_point_x = round((cur_point.x() - self._offset_x) / self._beat_width) * self._beat_width
        self.scene.player_cursor.setPos(rounded_point_x, 0)

    def mouseMoveEvent(self, event):
        pass

    def mouseReleaseEvent(self, event):
        pass


# noinspection PyPep8Naming
class DeleteMouseActions(MouseActions):
    def __init__(self, scene):
        self._start = None
        super().__init__(scene)

    def mousePressEvent(self, event):
        if self.scene.itemAt(event.scenePos(), QtGui.QTransform()) is None:
            cur_point = event.scenePos()
            self._start = cur_point

    def mouseMoveEvent(self, event):
        if self._start:
            cur_point = event.scenePos()
            path = QPainterPath()
            path.addRect(QRectF(self._start, cur_point))
            self.scene.setSelectionArea(path)

    def mouseReleaseEvent(self, event):
        rects_to_delete = [item for item in self.scene.selectedItems() if isinstance(item, MyQGraphicsRectItem)]
        for rect in rects_to_delete:
            self.scene.removeItem(rect)
            del rect
        self.scene.clearSelection()
        self._start = None


# действия мыши по пустой области на QGraphicsScene, будут меняться в зависимости от выбранного инструмента
# и подставляться в класс GraphicsScene
class TimelineMouseActionsWidget:
    def __init__(self):
        self.buffer: List[MyQGraphicsRectItem] = []
        self.current_actions_name: MouseActionsGroupName = MouseActionsGroupName.DRAW_TIMELINE

    def set_settings(self, beat_width_in_pixels: int, beat_height_in_pixels: int,
                 timelines_offset_x: int):
        self.beat_width_in_pixels = beat_width_in_pixels
        self.beat_height_in_pixels = beat_height_in_pixels
        self.timelines_offset_x = timelines_offset_x

    def change_grid_scale(self, beat_width_in_pixels: int, beat_height_in_pixels: int):
        self._build_actions(scene=self.scene, beat_width_in_pixels=beat_width_in_pixels,
                            beat_height_in_pixels=beat_height_in_pixels, timelines_offset_x=self.timelines_offset_x)

    def _build_actions(self, scene: QGraphicsScene, beat_width_in_pixels: int, beat_height_in_pixels: int,
                       timelines_offset_x: int):
        self.beat_width_in_pixels = beat_width_in_pixels
        self.beat_height_in_pixels = beat_height_in_pixels
        self.timelines_offset_x = timelines_offset_x
        self.scene = scene
        self.draw_timeline_actions = DrawTimelineMouseActions(scene=scene, beat_width_in_pixels=beat_width_in_pixels,
                                                              beat_height_in_pixels=beat_height_in_pixels,
                                                              timelines_offset_x=timelines_offset_x)
        self.select_elements_actions = SelectionMouseActions(scene=scene)
        self.set_player_cursor_actions = SetPlayerCursorActions(scene=scene, beat_width_in_pixels=beat_width_in_pixels,
                                                                timelines_offset_x=timelines_offset_x)
        self.delete_actions = DeleteMouseActions(scene=scene)
        self.actions_group_by_name: Dict[MouseActionsGroupName, MouseActions] = {
            MouseActionsGroupName.DRAW_TIMELINE: self.draw_timeline_actions,
            MouseActionsGroupName.SELECT: self.select_elements_actions,
            MouseActionsGroupName.DELETE: self.delete_actions,
            MouseActionsGroupName.SET_PLAYER_CURSOR: self.set_player_cursor_actions}

    def update_target_scene(self, new_scene: QGraphicsScene):
        self.scene = new_scene
        self._build_actions(scene=new_scene, beat_width_in_pixels=self.beat_width_in_pixels,
                            beat_height_in_pixels=self.beat_height_in_pixels,
                            timelines_offset_x=self.timelines_offset_x)

    def mousePressEventHandler(self, event):
        self.actions_group_by_name[self.current_actions_name].mousePressEvent(event)

    def mouseMoveEventHandler(self, event):
        self.actions_group_by_name[self.current_actions_name].mouseMoveEvent(event)

    def mouseReleaseEventHandler(self, event):
        self.actions_group_by_name[self.current_actions_name].mouseReleaseEvent(event)
