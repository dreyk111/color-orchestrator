from pathlib import Path

import pickle

from models.timeline import Checkpoint, Mode, Timeline, Song

with open("data/evo_with_editor2.pkl", "rb") as file:
    timelines = pickle.load(file)
s = Song(file=Path("data/EVO - Стать мечтой.mp3"), start_beat=2.5, bpm=149, timelines=timelines)

# s = Song(file=Path("data/Alan Walker, K-391  Sofia Carson feat. CORSAK - Different World.mp3"), start_beat=(1-0.05), bpm=130, timelines=timelines)
