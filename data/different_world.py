from pathlib import Path

from models.timeline import Checkpoint, Mode, Timeline, Song

group1 = [
    Checkpoint(level=50, beats=1),
    Checkpoint(level=20, beats=1),
    Checkpoint(level=50, beats=1),
    Checkpoint(level=100, beats=1),
]

group2 = [

]

start_group = [
    Checkpoint(level=0, beats=2, mode=Mode.LINEAR),
    Checkpoint(level=100, beats=2, mode=Mode.LINEAR),
]

start_groupqq = [
    Checkpoint(level=0, beats=2),
    Checkpoint(level=100, beats=2),
]

checkpoints1 = [
    Checkpoint(level=100, beats=0),
] + start_group * 888 + group1 * 8 + group1 * 8

checkpoints2 = start_group * 888 + group1 * 4 + start_group * 8

timelines = [Timeline(name="Led1", checkpoints=checkpoints1), Timeline(name="Led2", checkpoints=checkpoints2)]
s = Song(file=Path("data/Alan Walker, K-391  Sofia Carson feat. CORSAK - Different World.mp3"), start_beat=(1-0.05), bpm=130, timelines=timelines)
