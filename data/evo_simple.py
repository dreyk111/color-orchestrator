from pathlib import Path

from models.timeline import Checkpoint, Mode, Timeline, Song

checkpoints1 = [
    Checkpoint(level=100, beats=2),
    Checkpoint(level=0, beats=2),
] * 10

checkpoints2 = [
    Checkpoint(level=0, beats=2),
    Checkpoint(level=100, beats=2),
] * 10

checkpoints1 = [
    Checkpoint(level=100, beats=2),
    Checkpoint(level=0, beats=2),
]

checkpoints2 = [
    Checkpoint(level=0, beats=2),
    Checkpoint(level=100, beats=2),
]

timelines = [Timeline(name="Led1", checkpoints=checkpoints1), Timeline(name="Led2", checkpoints=checkpoints2)]
s = Song(file=Path("data/EVO - Стать мечтой.mp3"), start_time=0.8, bpm=149, timelines=timelines)
