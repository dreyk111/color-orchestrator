from pathlib import Path

import pickle

from models.timeline import Song

CUR_DIR = Path(__file__).resolve().parent

with open(CUR_DIR / "my_friends.pkl", "rb") as file:
    timelines = pickle.load(file)
s = Song(file=Path(CUR_DIR / "ODESZA - my friends never die.mp3"), start_time=5.6, bpm=340, timelines=timelines)
# start_time=5.9