from pathlib import Path

from models.timeline import Checkpoint, Mode, Timeline, Song

group = [
    Checkpoint(level=100, beats=2),
    Checkpoint(level=0, beats=2, mode=Mode.LINEAR),
    Checkpoint(level=50, beats=1),
    Checkpoint(level=20, beats=1),
    Checkpoint(level=50, beats=1),
    Checkpoint(level=20, beats=1),
]

checkpoints1 = [
    Checkpoint(level=100, beats=0),
    Checkpoint(level=0, beats=2, mode=Mode.LINEAR),
    Checkpoint(level=50, beats=2),
    Checkpoint(level=20, beats=2),
    Checkpoint(level=50, beats=2),
] + group * 10

checkpoints2 = [
    Checkpoint(level=0, beats=0),
    Checkpoint(level=100, beats=2, mode=Mode.LINEAR),
    Checkpoint(level=20, beats=2),
    Checkpoint(level=50, beats=2),
] + group * 10

timelines = [Timeline(name="Led1", checkpoints=checkpoints1), Timeline(name="Led2", checkpoints=checkpoints2)]
s = Song(file=Path("data/EVO - Стать мечтой.mp3"), start_beat=2.5, bpm=149, timelines=timelines)
