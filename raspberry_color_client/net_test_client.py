import pickle
import socket


HOST = '192.168.0.104'
PORT = 65432
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    while True:
        data = s.recv(1000000)
        if data:
            data_variable = pickle.loads(data)
            print(data_variable)
