import time
from copy import deepcopy
from threading import Thread
from typing import List
import RPi.GPIO as GPIO

from models.timeline import Timeline, Mode

SNAP_PER_SEC = 100

GPIO.setmode(GPIO.BCM)
# Disable "This channel is already in use" warnings
GPIO.setwarnings(False)


class TimelineRaspberryPlayer(Thread):
    def __init__(self, bpm: int, start_beat: float, ports: List[int]):
        Thread.__init__(self)
        self.bpm = bpm
        self.ports = ports
        self.start_beat = start_beat
        self.stop_flag = True
        self.exit_flag = False
        for port in self.ports:
            GPIO.setup(port, GPIO.OUT)
            GPIO.output(port, False)

    # play timelines
    def play_timelines(self, timelines: List[Timeline], additional_beat_shift=0):
        self.timelines = deepcopy(timelines)
        self.start_from_beat = additional_beat_shift
        self.stop_flag = False
        self.start_millis = time.time()  # in seconds
        self.sum_delta_from_start = [0 for t in timelines]  # Sum of played steps in seconds

    def set_color(self, timeline_index: int, hue: int, saturation: int, lightness: int):
        GPIO.output(self.ports[timeline_index], bool(lightness))

    # thread run
    def run(self):
        while not self.exit_flag:
            while not self.exit_flag:
                if not self.stop_flag:
                    break
                else:
                    time.sleep(1 / SNAP_PER_SEC)  # Waiting for input data to play - call play_timelines()
            if self.exit_flag:
                break
            time_between_beats = 60 / self.bpm  # in seconds
            # Перед началом подчищаем таймлайны, пропуская события до start_from_beat
            if not self.start_from_beat:
                # start_from_beat = 0, играем с самого начала
                # ждём начало песни start_timestamp
                # В режиме просмотра таймлайнов ждать до начала никогда не надо! Мы уже стартуем с первого бита!
                # delta_time = time_between_beats * self.s.start_beat
                # wait_to_time_millis = self.start_millis + delta_time
                # time.sleep(wait_to_time_millis - time.time())
                # self.sum_delta_from_start += delta_time
                pass
            else:
                for timeline_index, timeline in enumerate(self.timelines):
                    checkpoints = timeline.checkpoints
                    if not checkpoints:
                        continue
                    # если start_from_beat = 1 - начинаем с 1-го бита
                    sum_beats_from_start = checkpoints[0].beats
                    if sum_beats_from_start >= self.start_from_beat:
                        start_checkpoint_num = 0
                        checkpoints[0].beats = sum_beats_from_start - self.start_from_beat
                    else:
                        start_checkpoint_num = len(checkpoints) - 1
                        for n, p1 in enumerate(checkpoints[:-1]):
                            p2 = checkpoints[n+1]
                            if sum_beats_from_start + p2.beats >= self.start_from_beat:
                                start_checkpoint_num = n
                                p1.beats = sum_beats_from_start-self.start_from_beat
                                break
                            else:
                                sum_beats_from_start += p2.beats
                    timeline.checkpoints = checkpoints[start_checkpoint_num:]
            for timeline_index, timeline in enumerate(self.timelines):
                if not timeline.checkpoints:
                    continue
                # отображаем нужный уровень стартого бита
                GPIO.output(self.ports[timeline_index], bool(timeline.checkpoints[0].level))
            # Основной цикл проигрывания таймлайнов - event loop
            while not self.exit_flag:
                for timeline_index, timeline in enumerate(self.timelines):
                    checkpoints = timeline.checkpoints
                    if not checkpoints:
                        continue  # данный таймлайн уже закончился
                    current_checkpoint = checkpoints[0]
                    if len(checkpoints) == 1:  # last Checkpoint
                        next_checkpoint_level = 0  # последнее действие - выключить ленту
                    else:
                        next_checkpoint_level = checkpoints[1].level

                    delta_time = current_checkpoint.beats * time_between_beats
                    wait_to_time_millis = self.start_millis + self.sum_delta_from_start[timeline_index] + delta_time  # seconds timestamp
                    if current_checkpoint.mode == Mode.LINEAR:
                        delta_level = next_checkpoint_level - current_checkpoint.level
                        amount_of_steps = int(delta_time * SNAP_PER_SEC)
                        level_step = delta_level / amount_of_steps
                        cur_level = current_checkpoint.level
                        for i in range(amount_of_steps):
                            cur_level += level_step
                            # self.rect.setBrush(QColor.fromHsl(self.color_hue, 140, int(cur_level * 160 / 100))) TBD
                            if time.time() - wait_to_time_millis >= 0:
                                break  # для корректировки, более своевременного выхода из цикла
                            time.sleep(1 / SNAP_PER_SEC)
                    else:
                        time_to_sleep = wait_to_time_millis - time.time()
                        if time_to_sleep < 0:  # время данного чекпоинта прошло, удаляем его
                            GPIO.output(self.ports[timeline_index], bool(next_checkpoint_level))
                            self.sum_delta_from_start[timeline_index] += delta_time  # храним время отыгранных чекпоинтов, чтобы считать время для следующего заново
                            checkpoints.pop(0)

                # проверяем, если все таймлайны закончились:
                if not any(t.checkpoints for t in self.timelines):
                    break
                if self.stop_flag:
                    break
                time.sleep(1 / SNAP_PER_SEC)
            # Timeline закончились
            self.stop_flag = True
