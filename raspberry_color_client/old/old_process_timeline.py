import time
from threading import Thread

from models.timeline import Timeline, Song, Mode

SNAP_PER_SEC = 500


class PlayTimelineThread(Thread):
    def __init__(self, timeline: Timeline, s: Song, port, thread_name):
        print(timeline, s, port)
        Thread.__init__(self)
        self.timeline = timeline
        self.s = s
        self.port = port
        self.start_millis = time.time()  # in seconds
        self.sum_delta_from_start = 0  # Sum of steps in seconds
        self.thread_name = thread_name

    # FIXME: run ДОЛЖЕН быть запущен после init, тогда зачем run как отдельный этап?
    def run(self):
        time_between_beats = 60 / self.s.bpm  # in seconds
        # ждём начало песни start_timestamp
        delta_time = time_between_beats * self.s.start_beat
        wait_to_time_millis = self.start_millis + delta_time
        time.sleep(wait_to_time_millis - time.time())
        self.sum_delta_from_start += delta_time
        # начинаем проигрывать цветомузыку по таймлайну
        checkpoints = self.timeline.checkpoints
        if not checkpoints:
            exit(0)
        # отображаем нужный уровень после стартого бита
        self.port.start(checkpoints[0].level)
        print(self.thread_name, ":", int(self.sum_delta_from_start // time_between_beats), checkpoints[0].level)

        for n, p1 in enumerate(checkpoints):
            if n == len(checkpoints) - 1:  # last Checkpoint
                next_checkpoint_level = 0
            else:
                next_checkpoint_level = checkpoints[n + 1].level

            delta_time = p1.beats * time_between_beats
            wait_to_time_millis = self.start_millis + self.sum_delta_from_start + delta_time  # seconds timestamp
            if p1.mode == Mode.LINEAR:
                delta_level = next_checkpoint_level - p1.level
                amount_of_steps = int(delta_time * SNAP_PER_SEC)
                level_step = delta_level / amount_of_steps
                cur_level = p1.level
                for i in range(amount_of_steps):
                    cur_level += level_step
                    self.port.start(checkpoints[0].level)
                    if time.time() - wait_to_time_millis >= 0:
                        break  # для корректировки, более своевременного выхода из цикла
                    time.sleep(1 / SNAP_PER_SEC)
            else:
                time_to_sleep = wait_to_time_millis - time.time()
                if time_to_sleep > 0:
                    time.sleep(time_to_sleep)

            self.port.start(next_checkpoint_level)
            self.sum_delta_from_start += delta_time
            print(self.thread_name, ":", int(self.sum_delta_from_start // time_between_beats), next_checkpoint_level)
