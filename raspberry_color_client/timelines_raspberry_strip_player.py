import time
from copy import deepcopy
from threading import Thread
from typing import List, Optional

import numpy as np
from rpi_ws281x import *
import colorsys

from common.services.led_strip_frames_builder import LedStripFramesBuilder
from models.raspberry_run_config import StripRaspberryConfig
from models.timeline import Timeline, TimelinePattern

# My led strip config:
# PARTS_MARGIN = 10
# LEN_OF_PART = 10
# PARTS_NUMBER = 4
# LED strip configuration:
# LED_COUNT      =  PARTS_NUMBER*(PARTS_MARGIN+LEN_OF_PART)     # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 100     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


class TimelineRaspberryPlayer(Thread):
    def __init__(self, bpm: int, ports: List[int], strip_config: StripRaspberryConfig):
        Thread.__init__(self)
        self.bpm = bpm
        self.ports = ports
        self.stop_flag = True
        self.exit_flag = False
        self.strip_config = strip_config
        # Create NeoPixel object with appropriate configuration.
        self.LED_COUNT = self.strip_config.parts_number*(self.strip_config.len_of_part+self.strip_config.parts_margin)
        print("PARAMETERS GOT: ", self.LED_COUNT, self.strip_config.led_pin, LED_FREQ_HZ, LED_DMA, LED_INVERT, self.strip_config.led_brightness, LED_CHANNEL)
        strip = Adafruit_NeoPixel(self.LED_COUNT, self.strip_config.led_pin, LED_FREQ_HZ, LED_DMA, LED_INVERT, self.strip_config.led_brightness, LED_CHANNEL)
        # Intialize the library (must be called once before other functions).
        strip.begin()
        self.strip = strip

    def calculate_frames(self, timelines: List[Timeline], patterns: List[TimelinePattern]):
        print(f"Start all frames calculation, got: {len(patterns)} patterns")
        self.timelines = deepcopy(timelines)
        self.all_frames_by_pattern_number = {-1: self.fillFramesByTimelines(timelines)}
        # Calculate timelines
        self.all_frames_by_pattern_number.update({index+1: self.fillFramesByTimelines(pattern.timelines) for index, pattern in enumerate(patterns)})
        print("I calculated all frames!")

    # play timelines
    def play_timelines(self, pattern: Optional[int], additional_beat_shift=0, local_delay: float=0):
        self.pattern = pattern or -1
        self.start_from_beat = additional_beat_shift
        self.local_delay = local_delay
        # Задержка старта, для синхронизации запуска нескольких устройств
        time.sleep(self.local_delay)
        self.stop_flag = False
        self.start_millis = time.time()  # in seconds

    def set_color(self, timeline_index: int, h: int, s: int, l: int):
        """
        h: 0..359
        s: 0..100
        l: 0..100
        """
        (r_float, g_float, b_float) = colorsys.hsv_to_rgb(h/360, s/100, l/100)
        (r, g, b) = (r_float*255, g_float*255, b_float*255)
        self.setStripPartColor(self.strip, timeline_index, Color(int(r),int(g),int(b)))

    # thread run
    def run(self):
        while not self.exit_flag:
            while not self.exit_flag:
                if not self.stop_flag:
                    break
                else:
                    time.sleep(1 / self.strip_config.snap_per_sec)  # Waiting for input data to play - call play_timelines()
            if self.exit_flag:
                break
            time_from_start = time.time() - self.start_millis
            time_of_one_frame = 1 / self.strip_config.snap_per_sec
            if self.pattern == -1:
                current_frame_index = int(time_from_start / time_of_one_frame)
                if current_frame_index >= len(self.all_frames_by_pattern_number[self.pattern]):
                    if self.pattern == -1:
                        self.stop_flag = True
                        continue
            else:
                if not len(self.all_frames_by_pattern_number[self.pattern]):
                    self.stop_flag = True
                    continue  # timelines empty for this pattern
                current_frame_index = int(time_from_start / time_of_one_frame) % len(self.all_frames_by_pattern_number[self.pattern])

            current_frame = self.all_frames_by_pattern_number[self.pattern][current_frame_index]
            print(current_frame_index, current_frame)
            for pixel_index, pixel_color in enumerate(current_frame):
                self.strip.setPixelColor(pixel_index, int(pixel_color))
            self.strip.show()
            time.sleep(1 / self.strip_config.snap_per_sec)
            # Timeline закончились
            # self.stop_flag = True

    # Define functions which animate LEDs in various ways.
    # def colorWipe(self, strip, part_number: int, color, wait_ms=50):
    #     """Wipe color across display a pixel at a time."""
    #     start_pixel = part_number*(LEN_OF_PART+PARTS_MARGIN)
    #     stop_pixel = start_pixel+LEN_OF_PART
    #     for i in range(start_pixel, stop_pixel+1):
    #         strip.setPixelColor(i, color)
    #         strip.show()
    #         time.sleep(wait_ms/1000.0)

    def setStripPartColor(self, strip, part_number: int, color):
        start_pixel = part_number*(self.strip_config.len_of_part + self.strip_config.parts_margin)
        stop_pixel = start_pixel+self.strip_config.len_of_part
        print("Light on!", start_pixel, stop_pixel, color)
        for i in range(start_pixel, stop_pixel+1):
            strip.setPixelColor(i, color)
        strip.show()

    def theaterChase(self, strip, color, wait_ms=50, iterations=10):
        """Movie theater light style chaser animation."""
        for j in range(iterations):
            for q in range(3):
                for i in range(0, strip.numPixels(), 3):
                    strip.setPixelColor(i+q, color)
                strip.show()
                time.sleep(wait_ms/1000.0)
                for i in range(0, strip.numPixels(), 3):
                    strip.setPixelColor(i+q, 0)

    def fillFramesByTimelines(self, timelines: List[Timeline]) -> np.array:
        builder = LedStripFramesBuilder(timelines=timelines, additional_beat_shift=0,
                                        parts_margin=self.strip_config.parts_margin,
                                        len_of_part=self.strip_config.len_of_part,
                                        parts_number=self.strip_config.parts_number, frames_per_second=self.strip_config.snap_per_sec,
                                        bpm=self.bpm, color_cls=Color, )
        return builder.build()
