import time
import os, sys
import socket


sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__))))

#import RPi.GPIO as GPIO
from models.raspberry_run_config import RunRaspberryConfig
from raspberry_color_client.timelines_raspberry_strip_player import TimelineRaspberryPlayer


HOST = '192.168.5.22'
PORT = 65431

# GPIO.setmode(GPIO.BCM)
# Disable "This channel is already in use" warnings
# GPIO.setwarnings(False)

timelines_player = None
config = None
empty_messages_start_time = None

while True:
    print(f"Waiting for connection to server '{HOST}:{PORT}'...")
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((HOST, PORT))
            print("Successfully connected to server!")
            data = b""
            while True:
                try:
                    data_part = sock.recv(4096)
                except BlockingIOError:
                    continue
                current_message = data_part.decode("utf-8")
                if len(current_message) == 0:
                    cur_time = time.time()
                    if empty_messages_start_time and cur_time - empty_messages_start_time > 30:  # timeout for any healthcheck message
                        raise Exception("My timeout of connection receiving any message on 30 seconds")
                    else:
                        empty_messages_start_time = cur_time
                elif current_message == "^":
                    continue
                elif current_message[0] == "t":
                    if timelines_player:
                        bpm_text = ""
                        for symbol in current_message[1:]:
                            if symbol.isdigit():
                                bpm_text += symbol
                            else:
                                break
                        if bpm_text:
                            timelines_player.stop_flag = True
                            timelines_player.bpm = int(bpm_text)
                            timelines_player.calculate_frames(timelines=config.timelines[:len(config.ports)],
                                                              patterns=config.patterns)
                            timelines_player.stop_flag = False
                    data = b""
                elif current_message == "stop":
                    if timelines_player:
                        timelines_player.stop_flag = True
                    data = b""
                elif current_message == "~":
                    if timelines_player and config:
                        timelines_player.stop_flag = True
                        timelines_player.play_timelines(pattern=None, additional_beat_shift=config.additional_beat_shift, local_delay=config.start_time)
                    data = b""
                elif current_message.isdigit():  # Pattern play
                    if timelines_player and config:
                        timelines_player.stop_flag = True
                        timelines_player.play_timelines(pattern=int(current_message), additional_beat_shift=config.additional_beat_shift, local_delay=config.start_time)
                    data = b""
                else:
                    data += data_part
                    full_message = data.decode("utf-8")
                    try:
                        config = RunRaspberryConfig.from_json(full_message)
                        print("Successfully received message!")
                        # конфиг загружен успешно (в первый запуск создаётся тред с настройками песни, со следующего запуска эти настройки будут игнорироваться)
                        if not timelines_player:
                            timelines_player = TimelineRaspberryPlayer(config.bpm, config.ports, config.led_strip)
                            timelines_player.start()
                        timelines_player.calculate_frames(timelines=config.timelines[:len(config.ports)], patterns=config.patterns)
                        data = b""
                    except Exception as e:
                        print(f"JSON doesn't received fully, waiting next data part, len of last part: {len(current_message)}")
                        pass
                time.sleep(0.01)
    except Exception as e:
        print(f"Catch {str(e)}, skipping for trying to connect again after 2 seconds...")
        time.sleep(2)
